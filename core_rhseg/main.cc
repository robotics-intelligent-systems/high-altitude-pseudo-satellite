/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<<
   >>>>
   >>>> 	Main program for the rhseg program - with gtkmm and GDAL dependencies
   >>>>
   >>>>  Private:
   >>>> 	main(int argc, char **argv)
   >>>> 	usage()
   >>>> 	help()
   >>>>
   >>>>   Static:
   >>>>   Public:
   >>>>
   >>>>       Written: January 28, 2009
   >>>> Modifications: May 12, 2009: Modified user interface and data I/O for compatibility with ILIADS project needs.
   >>>>                July 8, 2009: Final clean-up of code for release
   >>>>                July 24, 2009: Minor bug fix for hseg_out_nregions and hseg_out_thresholds
   >>>>                September 24, 2009: Bug fix for floating point input in parallel version
   >>>>                October 6, 2009: Added robustness to the reading of parameter values from parameter files
   >>>>                December 2, 2009 - Replaced use of strftime (with its time_buffer) with ctime.
   >>>>
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<< */

#include "main.h"
#include "hseg.h"
#include "params/params.h"
#ifdef GTKMM
#include "params/initialGUI.h"
#include "params/outputFileGUI.h"
#include "params/paramsGUI.h"
#include "params/finalGUI.h"
#include "status/statustracker.h"
#endif
#ifdef GDAL
#include "image/image.h"
#include <gdal_priv.h>
#endif // GDAL
#include <iostream>
#include <ctime>
#ifdef GTKMM
#include <gtkmm/main.h>
#include <gtkmm/messagedialog.h>
#include <pthread.h>
#endif

using namespace std;
using namespace HSEGTilton;
#ifdef GDAL
using namespace CommonTilton;
#endif

//Globals
Params params("Version 1.47, December 2, 2009");
oParams oparams;

#ifdef GDAL
extern Image inputImage;
extern Image maskImage;
extern Image regionMapInImage;
#endif

// Forward function declarations
void usage();
void help();

/*-----------------------------------------------------------
|
|  Routine Name: main - Main program for Hierarchical Segmentation
|
|  Purpose: Declares program parameters, reads parameter file and initializes program parameters,
|           opens log file (if requested), calls the hseg function and returns an exit status.
|
|  Input from command line (no input from command line for GUI version) - parameter_file_name
|
|       Returns: EXIT_SUCCESS on success, EXIT_FAILURE on failure
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt,MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: December 16, 2002.
| Modifications: May 20, 2003 - Added option to initialize with Classical Region Growing.
|                May 30, 2003 - Added classic_pred flag.  If TRUE, the Classical Region Growing
|                               predicate is used.  If FALSE, the region-based discriminate of
|                               HSEG is used.
|                August 19, 2003 - Reorganized program and added user controlled byteswapping
|                October 18, 2003 - Added boundary_npix_file option
|                October 31, 2003 - Added subrlblmap_file and nb_subregions_file options
|                November 5, 2003 - Revised Params structure
|                November 24, 2003 - Changed "sub" to "conn" (for connected regions) and added
|                                    full set of outputs for connected regions
|                March 2, 2004 - Changed extmean_flag to extmean_wght
|                March 2, 2004 - Added option to output boundary_map
|                April 2, 2004 - Added mask_value parameter
|                June 3, 2004 - Modified BSMSE and BMMSE to take square root
|                June 9, 2004 - Removed extmean option (ineffective)
|                March 1, 2005 - Updated and rearranged help information
|                March 10, 2005 - Added checks to prevent overwriting input files with any program output
|                May 9, 2005 - Added code for integration with VisiQuest
|                May 25, 2005 - Added temporary file I/O for faster processing of large data sets
|                August 9, 2005 - Added "conv_criterion" and "gstd_dev_crit" parameters
|                August 9, 2005 - Changed "rthreshlist" to "rconv_critlist"
|                October 14, 2005 - Added slice dimension (extension to three-dimensional analysis)
|                May 2, 2006 - Added scale and offset input parameters (for input data)
|                October 31, 2007 - Removed "conv_criterion," "conv_factor," "gdissim_crit" and "gstd_dev_crit"
|                                   parameters, and added "gdissim_flag" parameter
|                October 31, 2007 - Changed "rconv_critlist" back to "rthreshlist"
|                October 31, 2007 - Removed VisiQuest related code
|                November 9, 2007 - Combined region feature output into region_classes file
|                May 9, 2008 - Reorganized source code, including adding the "Params" object class.
|                July 28, 2008 - Added copyright notice.
|                August 1, 2008 - Modified function of "region_std_dev" parameter.
|                August 8, 2008 - Added hseg_out_thresholds and hseg_out_nregions parameters.
|                September 10, 2008 - Added percent completion (status) tracking.
|
------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  int status = true;

#ifdef GTKMM
  Gtk::Main kit(argc, argv);
  Glib::ustring strTime = " ";
  Glib::ustring strMessage = " ";
#endif

#ifdef GDAL
  GDALAllRegister();
#else  // NOTE: Should always use GDAL!
  cout << "ERROR: GDAL must be used in rhseg." << endl;
  return EXIT_FAILURE;
#endif // GDAL

  if (argc == 1)
  {
#ifdef GTKMM
    params.gtkmm_flag = true;
    InitialGUI initialGUI;
    Gtk::Main::run(initialGUI);
    status = params.status;
    if (status == 1)
    {
      OutputFileGUI outputFileGUI;
      Gtk::Main::run(outputFileGUI);
      status = params.status;
    }
    if (status == 1)
    {
      ParamsGUI paramsGUI;
      Gtk::Main::run(paramsGUI);
      status = params.status;
    }
    if (status == 3)
      return EXIT_SUCCESS;
    if (status == 2)
    {
      if (params.debug > 0)
      {
        params.log_fs.open(params.log_file.c_str(),ios_base::out);
        status = params.log_fs.is_open();
      }
    }
#else
    usage();
    cout << "ERROR: Need parameter file as argument." << endl;
    return EXIT_FAILURE;
#endif
  }
  else if ((strncmp(argv[1],"-v",2) == 0) || (strncmp(argv[1],"-version",8) == 0))
  {
    params.print_version();
    cout << "For help information: rhseg -h or rhseg -help" << endl << endl;
    return EXIT_SUCCESS;
  }
  else if ((strncmp(argv[1],"-h",2) == 0) || (strncmp(argv[1],"-help",5) == 0))
  {
    help();
    return EXIT_SUCCESS;
  }
  else if (strncmp(argv[1],"-",1) == 0)
  {
    usage();
    cout << "ERROR: The parameter file name cannot start with an \"-\"" << endl;
    return EXIT_FAILURE;
  }
  else
  {
    if (argc != 2)
    {
      usage();
      cout << "ERROR: Incorrect number of parameters on command line" << endl;
      return EXIT_FAILURE;
    }
    else
    {
      status = params.read_init(argv[1]);
      if (status)
      {
        if (params.debug > 0)
        {
          params.log_fs.open(params.log_file.c_str(),ios_base::out);
          status = params.log_fs.is_open();
        }
      }
      else
      {
        usage();
        cout << "ERROR: Error reading parameter file (read_init)" << endl;
        return EXIT_FAILURE;
      }
      if (status)
        status = params.read(argv[1]);
      else
      {
        usage();
        cout << "ERROR: Error opening log file: " << params.log_file << endl;
        return EXIT_FAILURE;
      }
    }
  }

  time_t now;
  if (status)
  {
    now = time(NULL);
    if (params.debug > 0)
    {
      params.log_fs << endl << "Started processing (hseg/rhseg) at : " << ctime(&now) << endl;
#if (defined(WINDOWS) || defined(CYGWIN))
      params.log_fs << endl << "Copyright (C) 2006 United States Government as represented by the" << endl;
#else
      params.log_fs << endl << "Copyright \u00a9 2006 United States Government as represented by the" << endl;
#endif
      params.log_fs << "Administrator of the National Aeronautics and Space Administration." << endl;
      params.log_fs << "No copyright is claimed in the United States under Title 17, U.S. Code." << endl;
      params.log_fs << "All Other Rights Reserved." << endl;
    }
    if (!params.gtkmm_flag)
    {
#if (defined(WINDOWS) || defined(CYGWIN))
     cout << endl << "Copyright (C) 2006 United States Government as represented by the" << endl;
#else
     cout << endl << "Copyright \u00a9 2006 United States Government as represented by the" << endl;
#endif
     cout << "Administrator of the National Aeronautics and Space Administration." << endl;
     cout << "No copyright is claimed in the United States under Title 17, U.S. Code." << endl;
     cout << "All Other Rights Reserved." << endl;
     cout << endl << "Started processing (hseg/rhseg) at:  " << ctime(&now) << endl;
    }
#ifdef GTKMM
    else
    {
      strTime = ctime(&now);
      strMessage = "Started processing (hseg/rhseg) at:  " + strTime;
    }
#endif
  }
  else
  {
    cout << "ERROR: Error reading parameter file (read)" << endl;
    return EXIT_FAILURE;
  }

// Calculate program parameters
  if (status)
    status = params.calc();

  if (status)
  {
// Print program parameters
    if (params.debug > 0)
      params.print();

#ifdef GDAL
  // create classLabelsMapImage, boundaryMapImage and objectLabelsMapImage
    int nbands = 1;
    GDALDataType data_type = GDT_UInt32;
    Image classLabelsMapImage, boundaryMapImage, objectLabelsMapImage;
    classLabelsMapImage.create_geo_copy(params.class_labels_map_file,inputImage,nbands,data_type,"ENVI");
    classLabelsMapImage.close();
    if (params.boundary_map_flag)
    {
      data_type = GDT_Byte;
      boundaryMapImage.create_geo_copy(params.boundary_map_file,inputImage,nbands,data_type,"ENVI");
      boundaryMapImage.close();
    }
    if (params.object_labels_map_flag)
    {
      data_type = GDT_UInt32;
      objectLabelsMapImage.create_geo_copy(params.object_labels_map_file,inputImage,nbands,data_type,"ENVI");
      objectLabelsMapImage.close();
    }
#endif

// Call hseg function to execute hseg algorithm
    if (!params.gtkmm_flag)
      status = hseg();
#ifdef GTKMM
    else
    {
      pthread_t pthread;
      int return_code, thread_nb = 0;
      return_code = pthread_create(&pthread, NULL, hseg_thread, (void *) thread_nb);
      if (return_code)
      {
        strMessage += "\nError returned from pthread_create()\n";
        Gtk::MessageDialog end_dialog(strMessage, false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);

        end_dialog.run();
        return EXIT_FAILURE;
      }
      StatusTracker statusTracker;
      Gtk::Main::run(statusTracker);
      status = params.gtkmm_flag;
      params.gtkmm_flag = true;
    }
#endif
  }

#ifdef GDAL
  inputImage.close();
  if (params.mask_flag)
    maskImage.close();
  if (params.region_map_in_flag)
    regionMapInImage.close();
#endif

  if (status)
  {
    now = time(NULL);
    if (params.debug > 0)
      params.log_fs << endl << "Successful completion of hseg/rhseg at:  " << ctime(&now) << endl;
    if (!params.gtkmm_flag)
    {
      cout << endl << "Successful completion of hseg/rhseg at:  " << ctime(&now) << endl;
    }
#ifdef GTKMM
    else
    {
      strTime = ctime(&now);
      strMessage += "\nSuccessful completion of hseg/rhseg at:  " + strTime;
      FinalGUI finalGUI(strMessage);
      Gtk::Main::run(finalGUI);
      status = params.status;
    }
#endif
  }
  else
  {
    if (params.debug > 0)
      params.log_fs << endl << "The hseg/rhseg program terminated improperly." << endl;
    if (!params.gtkmm_flag)
      cout << endl << "The hseg/rhseg program terminated improperly." << endl;
#ifdef GTKMM
    else
    {
      strMessage = "\nThe hseg/rhseg program terminated improperly\n";
      Gtk::MessageDialog end_dialog(strMessage, false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
      end_dialog.run();
    }
#endif
  }
  
  if (params.log_fs.is_open())
    params.log_fs.close( );

#ifdef GTKMM
  if (params.gtkmm_flag)
    pthread_exit(NULL);
#endif

  if (status)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}

/*-----------------------------------------------------------
|
|  Routine Name: usage - Usage function
|
|       Purpose: Informs user of proper usage of program when mis-used.
|
|         Input:
|
|        Output:
|
|       Returns: (void)
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt,MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: December 16, 2002.
| Modifications: May 9, 2008 - Updated usage text.
|                May 30, 2008 - Updated usage text.
|
------------------------------------------------------------*/
void usage()
{
  cout << endl << "Usage: " << endl << endl;
#ifdef GTKMM
  cout << "rhseg" << endl;
  cout << "or" << endl;
#endif
  cout << "rhseg parameter_file_name" << endl << endl;
  cout << "For help information: rhseg -h or rhseg -help" << endl;
  cout << "For version information: rhseg -v or rhseg -version" << endl;

#if (defined(WINDOWS) || defined(CYGWIN))
  cout << endl << "Copyright (C) 2006 United States Government as represented by the" << endl;
#else
  cout << endl << "Copyright \u00a9 2006 United States Government as represented by the" << endl;
#endif
  cout << "Administrator of the National Aeronautics and Space Administration." << endl;
  cout << "No copyright is claimed in the United States under Title 17, U.S. Code." << endl;
  cout << "All Other Rights Reserved." << endl << endl;

  return;
}

/*-----------------------------------------------------------
|
|  Routine Name: help - Help function
|
|       Purpose: Provides help information to user on program parameters
|
|         Input:
|
|        Output:
|
|       Returns: (void)
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt,MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: December 16, 2002.
| Modifications: March 1, 2005 - Reorganized and updated help text.
|                November 9, 2007 - Updated help text.
|                May 9, 2008 - Updated help text.
|                May 30, 2008 - Updated help text.
|                May 12, 2009: Modified user interface and data I/O for compatibility with ILIADS project needs.
|
------------------------------------------------------------*/
void help()
{
#ifdef GTKMM
  cout << endl << "The rhseg progam can be called in two different manners:" << endl;
  cout << endl << "rhseg" << endl;
  cout << endl << "(where the program parameters are entered via a Graphical User Interface)" << endl;
  cout << endl << "or" << endl;
  cout << endl << "rhseg parameter_file_name" << endl;
  cout << endl << "(where the program parameters are read from a parameter file)" << endl;
  cout << endl << "In the later case, \"parameter_file_name\" is the name of the input parameter" << endl;
#else
  cout << endl << "The rhseg progam is called in the following manner:" << endl;
  cout << endl << "rhseg_setup parameter_file_name" << endl;
  cout << endl << "where \"parameter_file_name\" is the name of the input parameter" << endl;
#endif
  cout << "file. For contents see below." << endl;
  cout << endl << "For this help: rhseg -h or rhseg -help" << endl;
  cout << endl << "For version information: rhseg -v or rhseg -version";
  cout << endl;
#ifdef GTKMM
  cout << endl << "The (optional) parameter file consists of entries of the form:" << endl;
#else
  cout << endl << "The parameter file consists of entries of the form:" << endl;
#endif
  cout << endl << "-parameter_name parameter_value(s)" << endl;

  fprintf(stdout,"\nThe following parameters may be specified in the input parameter file:\n\n");
  fprintf(stdout,"Input Files:\n"
"-input_image		(string)	Input image data file name (required)\n"
"-mask			(string)	Input data mask file name (optional,\n"
"					default = {none})\n"
"-region_map_in		(string)	Input region map file (optional,\n"
"					default = {none})\n");

  fprintf(stdout,"\nOther Required Parameters:\n"
"-spclust_wght		(float)		Relative importance of spectral\n"
"					clustering versus region growing\n"
"					(0.0 <= spclust_wght <= 1.0, no default)\n"
"-dissim_crit		(int)		Dissimilarity Criterion\n"
"					  1. \"1-Norm,\"\n"
"					  2. \"2-Norm,\"\n"
"					  3. \"Infinity Norm,\"\n"
"					  4. \"Spectral Angle Mapper,\"\n"
"					  5. \"Spectral Information Divergence,\"\n"
#ifdef MSE_SQRT
"					  6. \"Square Root of Band Sum Mean\n"
"					     Squared Error,\"\n"
"					  7. \"Square Root of Band Maximum Mean\n"
"					     Squared Error,\"\n"
#else
"					  6. \"Band Sum Mean Squared Error,\"\n"
"					  7. \"Band Maximum Mean Squared Error,\"\n"
#endif
"					  8. \"Normalized Vector Distance,\"\n"
"					  9. \"Entropy,\"\n"
"					 10. \"SAR Speckle Noise Criterion,\"\n"
//"					 11. \"Feature range.\"\n"
#ifdef MSE_SQRT
"					 (default: 6. \"Square Root of Band\n"
"					  Sum Mean Squared Error\")\n"
#else
"					 (default: 6.\"Band Sum Mean Squared Error\")\n"
#endif
"-log			(string)	Output log file (no default)\n\n");

  fprintf(stdout,"\nThe following optional parameters specify the scaling of the input image data:\n\n");
  fprintf(stdout,"-scale			(double)	Comma delimited list of input data\n"
"					scale factors (specify one value per band,\n"
"					default = 1.0 for each band)\n"
"-offset			(double)	Comma delimited list of input data\n"
"					offset factors (specify one value per band,\n"
"					default = 0.0 for each band)\n\n");

  fprintf(stdout,"Output Files (with default names - see User's Manual):\n"
"-class_labels_map	(string)	Output class labels map file name (required)\n"
"-boundary_map		(string)	Output hierarchical boundary map file name\n"
"					(optional)\n"
"-region_classes		(string)	Output region classes file name (required)\n"
"-oparam			(string)	Output parameter file name (required,\n"
"					default = \"\'input_image_file_name\'.oparam\")\n\n");

   fprintf(stdout,"If spclust_wght > 0.0, other optional output files (also with default names) are:\n"
"-object_labels_map	(string)	Output object labels map file name\n"
"-region_objects		(string)	Output region objects file name\n"
"NOTE: Both must be specified if either is specified!\n");

  fprintf(stdout,"\nThe following parameters select the optional contents of the required output\n"
"region_classes file and the optional output region_objects file (above):\n\n") ;
  fprintf(stdout,"-region_sum		(bool)		Flag to request the inclusion of the region\n"
"					sum feature values (also sumsq and sumxlogx,\n"
"					if available). (true (1) or false (0),\n"
"					default = true if nbands < 20,\n"
"					default = false otherwise)\n"
"-region_std_dev		(bool)		Flag to request the inclusion of the region\n"
"					standard deviation feature values.\n"
"					(true (1) or false (0), default = false.)\n"
"-region_boundary_npix	(bool)		Flag to request the inclusion of the region\n"
"					boundary number of pixels values.\n"
"					(true (1) or false (0),default = false)\n"
"-region_threshold	(bool)		Flag to request the inclusion of the merge\n"
"					threshold for the most recent merge for\n"
"					each region.\n"
"					(true (1) or false (0), default = false)\n"
/* Always assumed true!!
"-region_nghbrs_list	(bool)		Flag to request the inclusion of the list\n"
"					of the region classes neighboring each\n"
"					region class.\n"
"					(true (1) or false (0), default = false)\n"
*/
"-region_nb_objects	(bool)		Flag to request the inclusion of the number\n"
"					of region objects contained in each region\n"
"					class. (true (1) or false (0), default =\n"
"					true if spclust_wght != 0.0 and both\n"
"					\"-object_labels_map\" and \"-region_objects\"\n"
"					are specified, and false otherwise.\n"
"					A true value is allowed only when\n"
"					spclust_wght != 0.0 and both\n"
"					\"-object_labels_map\" and \"-region_objects\"\n"
"					are specified. User provided value ignored\n"
"					and set to true if \"-region_objects_list\"\n"
"					(below) is true.)\n"
"-region_objects_list	(bool)		Flag to request the inclusion of the list of\n"
"					region objects contained in each region\n"
"					class. (true (1) or false (0), default =\n"
"					true if spclust_wght != 0.0 and both\n"
"					\"-object_labels_map\" and \"-region_objects\"\n"
"					are specified, and false otherwise.\n"
"					A true value is allowed only when\n"
"					spclust_wght != 0.0 and both\n"
"					\"-object_labels_map\" and \"-region_objects\"\n"
"					are specified.)\n");

  fprintf(stdout,"\nThe following parameters are recommended for variation by all users\n");
  fprintf(stdout,"(defaults provided):\n\n");
  fprintf(stdout,"-conn_type		(int)		Neighbor connectivity type\n"
"					  1-D Case:\n"
"					    1. \"Two Nearest Neighbors\",\n"
"					    2. \"Four Nearest Neighbors\",\n"
"					    3. \"Six Nearest Neighbors\",\n"
"					    4. \"Eight Nearest Neighbors\",\n"
"					    (default: 1. \"Two Nearest Neighbors\")\n"
"					  2-D Case:\n"
"					    1. \"Four Nearest Neighbors\",\n"
"					    2. \"Eight Nearest Neighbors\",\n"
"					    3. \"Twelve Nearest Neighbors\",\n"
"					    4. \"Twenty Nearest Neighbors\",\n"
"					    5. \"Twenty-Four Nearest Neighbors\",\n"
"					    (default: 2. \"Eight Nearest Neighbors\")\n"
"-chk_nregions  		(unsigned int)	Number of regions at which hierarchical\n"
"					segmentation output is initiated (2 <=\n"
"					chk_nregions < %d, default = 64 if\n"
"					\"hseg_out_nregions\" and \"hseg_out_thresholds\"\n"
"					not specified)\n"
"-hseg_out_nregions 	(unsigned int)	The set of number of region levels\n"
"					at which hierarchical segmentation outputs\n"
"					are made (a comma delimited list)\n"
"-hseg_out_thresholds	(float)		The set of merge thresholds at which\n"
"					hierarchical segmentation outputs are made\n"
"					(a comma delimited list)\n"
"NOTE: -chk_nregions, -hseg_out_nregions and -hseg_out_thresholds are mutually\n"
"exclusive. If more than one of these are specified, the last one specified\n"
"controls and the previous specifications are ignored. However, -hseg_out_nregions\n"
"and -hseg_out_thresholds may not be specified for -rnb_levels > 1.\n\n"
"-conv_nregions		(unsigned int)	Number of regions for final convergence.\n"
"					(0 < conv_nregions < %d, default = 2)\n"
"-gdissim		(bool)		Flag to request output of global\n"
"					dissimilarity values in the log file.\n"
"					true (1) or false (0), default = false)\n",
					USHRT_MAX,USHRT_MAX);

  fprintf(stdout,"\nThe default values should be used for the following parameters,\n");
  fprintf(stdout,"except in special circumstances (defaults provided):\n\n");
  fprintf(stdout,"-debug			(int)		Debug option - controls log file\n"
"					outputs, (0 < debug < 255, default = 1)\n"
"-normind		(int)		Image normalization type\n"
"					  1.\"No Normalization\",\n"
"					  2.\"Normalize Across Bands\",\n"
"					  3.\"Normalize Bands Separately\"\n"
"					(default: 2. \"Normalize Across Bands\")\n"
"-init_threshold		(float)		Threshold for initial fast region merging\n"
#ifdef CLRG
"					by a pixel oriented first merge process.\n"
#else
"					by a region oriented first merge process.\n"
#endif
"					(default = 0.0)\n"
"-std_dev_wght		(float)		Weight for standard deviation spatial\n"
"					feature (std_dev_wght >= 0.0,\n"
"					default = 0.0)\n"
"-min_npixels		(unsigned int)	For regions smaller than this minimum number\n"
"					of pixels, the dissimilarity function is\n"
"					adjusted to favor merging.\n"
"					(for dissim_crit = 6, 7, 9, 10 or 11:\n"
"					default = 0; for dissim_crit = 1, 2, 3, 4,\n"
"					5 or 8: default = 200)\n"
"-rnb_levels		(int)		Number of recursive levels\n"
"					(0 < rnb_levels < 255, default calculated)\n"
"-ionb_levels		(int)		Recursive level at which data I/O is\n"
"					performed (0 < ionb_levels <= rnb_levels,\n"
"					default calculated)\n"
"-min_nregions		(unsigned int)	Number of regions for convergence at\n"
"					intermediate stages.\n"
"					(0 < min_nregions < %d,\n"
"					default calculated)\n"
"-spclust_start		(unsigned int)	Number of regions at and below which\n"
"					full spectral clustering is always\n"
"					utilized (0 <= spclust_start < %d,\n"
"					default calculated)\n",USHRT_MAX,USHRT_MAX);

#if (defined(WINDOWS) || defined(CYGWIN))
  cout << endl << "Copyright (C) 2006 United States Government as represented by the" << endl;
#else
  cout << endl << "Copyright \u00a9 2006 United States Government as represented by the" << endl;
#endif
  cout << "Administrator of the National Aeronautics and Space Administration." << endl;
  cout << "No copyright is claimed in the United States under Title 17, U.S. Code." << endl;
  cout << "All Other Rights Reserved." << endl << endl;

  return;
}
