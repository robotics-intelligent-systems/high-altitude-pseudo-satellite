/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<<
   >>>>
   >>>>       Purpose: Include file for the main program.
   >>>>                Used by the rhseg program for compiler directives,
   >>>>                include files and definitions
   >>>>
   >>>>    Written By: James C. Tilton, NASA GSFC, Mail Code 606.3, Greenbelt, MD 20771
   >>>>                James.C.Tilton@nasa.gov
   >>>>
   >>>>       Written: January 28, 2009
   >>>> Modifications: May 12, 2009: Modified user interface and data I/O for compatibility with ILIADS project needs.
   >>>>                July 8, 2009: Final clean-up of code for release
   >>>>                September 24, 2009: Bug fix for floating point input in parallel version
   >>>>
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<< */

#ifndef MAIN_H
#define MAIN_H

#include "defines.h"

#define GTKMM
//#undef GTKMM         // GTKMM may be undefined if a GUI is not desired as an option.
#define GDAL           // Do not undefine GDAL! rhseg MUST use GDAL!
#define CONSOLIDATED   // Do not undefine!
#define RHSEG_RUN      // Do not undefine!
#define OPARAM_FILE ".oparam"     // Do not undefine!

#endif /* MAIN_H */
