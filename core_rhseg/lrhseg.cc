/*-----------------------------------------------------------
|
|  Routine Name: lrhseg
|
|       Purpose: Recursive call to lhseg
|
|         Input: region_label_offset (Offset to be added to region_label before exiting)
|                recur_level      (Current recursive level of this task)
|                section          (Section or window processed by this call to lrhseg)
|                stride           (Number of data sections (or tasks) covered by each child task)
|                nb_sections      (Number of data sections (or tasks) covered by this task)
|                ncols            (Current number of columns in data being processed)
|                nrows            (Current number of rows in data being processed)
|                spatial_data     (Class which holds information pertaining to input and output spatial data)
|                pixel_data       (Class which holds information pertaining to the pixels processed by this task)
|                
|        Output:
|
|         Other: region_classes      (Class which holds region related information)
|                global_nregions  (Current number of regions globally)
|                max_threshold    (Maximum merging threshold encountered)
|                temp_data        (buffers used in communications between parallel tasks)
|
|       Returns: nregions         (Current number of regions for processed section(s))
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt, MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: on December 19, 2002.
| Modifications: January 29, 2003 - Added connected component labeling for (spclust_flag == false) case.
|                February 10, 2003 - Changed region_index to region_label
|                May 20, 2003 - Added option to initialize with Classical Region Growing.
|                June 9, 2003 - Changed certain lists to sets for efficiency.
|                August 27, 2003 - Reorganized program and added user controlled byteswapping
|                September 2, 2003 - Implemented Classical Region Growing for intermediate recursion levels.
|                September 24, 2003 - Eliminated use of the index_data array
|                November 7, 2003 - Modified Params structure and made class member variables all private
|                December 23, 2004 - Changed region label from short unsigned int to unsigned int
|                May 31, 2005 - Added temporary file I/O for faster processing of large data sets
|                October 14, 2005 - Added slice dimension (extension to three-dimensional analysis)
|                February 15, 2007 - Modified region initialization to allow option for initial fast merge process
|                May 12, 2008 - Revised to work with globally defined Params and oParams class objects
|                September 5, 2008 - Modified lrhseg call vis-a-vis nregions & max_threshold
|
------------------------------------------------------------*/

#include "hseg.h"
#include "params/params.h"
#include "spatial/spatial.h"
#include "pixel/pixel.h"
#include "region/region_class.h"
#include "index/index.h"
#include <iostream>

extern HSEGTilton::Params params;
extern HSEGTilton::oParams oparams;

namespace HSEGTilton
{
 unsigned int lrhseg(const unsigned int& region_label_offset,
                     const short unsigned int& recur_level, const short unsigned int& section,
                     const short unsigned int& stride, const short unsigned int& nb_sections,
                     const int& ncols, const int& nrows, unsigned int& global_nregions,
                     double& max_threshold, Spatial& spatial_data, vector<Pixel>& pixel_data, 
                     vector<RegionClass>& region_classes, vector<RegionClass *>& nghbr_heap, 
                     vector<RegionClass *>& region_heap, Temp& temp_data)
 {
   int row, col, pixel_ncols, pixel_nrows;
   unsigned int region_label, pixel_index, npixels, total_npixels;

   pixel_ncols = ncols;
   if (ncols > params.pixel_ncols)
     pixel_ncols = params.pixel_ncols;
   pixel_nrows = nrows;
   if (nrows > params.pixel_nrows)
     pixel_nrows = params.pixel_nrows;
   npixels = pixel_ncols*pixel_nrows;
   total_npixels = ncols*nrows;
   unsigned int pixel_data_size = pixel_data.size( );

   if (params.debug > 1)
   {
     params.log_fs << endl << "Entering lrhseg with recur_level = " << recur_level;
#ifdef DEBUG
     params.log_fs << ", first_sec = " << section << ", stride = " << stride;
     params.log_fs << "," << endl << "nb_sections = " << nb_sections;
     params.log_fs << ", npixels directly accessible by this task = " << npixels << "," << endl;
     params.log_fs << "total number of pixels this processing window = " << total_npixels << ", ";
     params.log_fs << "and region_label_offset = " << region_label_offset << "." << endl;
#else
     params.log_fs << endl;
#endif
   }

   if ((params.nb_sections > 1) && (recur_level == params.inb_levels))
     restore_pixel_data( section, pixel_data, temp_data);

   if (recur_level >= params.inb_levels)
   {
     if (params.debug > 3)
     {
       params.log_fs << endl << "Entering lrhseg, dump of the pixel data:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
         pixel_data[pixel_index].print(pixel_index);
     }
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "Entering lrhseg, dump of the region labeling of pixel_data:" << endl << endl;
         for (row = 0; row < nrows; row++)
         {
           for (col = 0; col < ncols; col++)
           {
             pixel_index = col + row*ncols;
             pixel_data[pixel_index].print_region_label();
           }
           params.log_fs << endl;
         }
         params.log_fs << endl;
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "Entering lrhseg, dump of the pixel data region labels:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
         if (pixel_data[pixel_index].get_region_label( ) != 0)
         {
           params.log_fs << "Element " << pixel_index << " is associated with region label ";
           pixel_data[pixel_index].print_region_label();
           params.log_fs << endl;
         }
     }
#endif // #ifdef DEBUG
   }

   unsigned int region_classes_size, nregions, init_nregions = 0;
   unsigned int region_index;
   map<unsigned int,unsigned int> region_relabel_pairs;

   if (recur_level == params.rnb_levels)
   {
    // This is the deepest level of recursion.
    // Make the region_label numbering compact in pixel_data.
     nregions = 0;
     init_nregions = 0;
     map<unsigned int,unsigned int>::iterator region_relabel_pair_iter;
     bool do_region_class_relabel_flag = false;
     for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
     {
       if (pixel_data[pixel_index].get_mask())
         init_nregions++;
       region_label = pixel_data[pixel_index].get_region_label( );
       if (region_label != 0)
       {
         region_relabel_pair_iter = region_relabel_pairs.find(region_label);
         if (region_relabel_pair_iter != region_relabel_pairs.end( ))
         {
           region_label = (*region_relabel_pair_iter).second;
         }
         else
         {
           region_relabel_pairs.insert(make_pair(region_label,++nregions));
           if (region_label != nregions)
             do_region_class_relabel_flag = true;
           region_label = nregions;
         }
         pixel_data[pixel_index].set_region_label(region_label);
       }
     }
     region_relabel_pairs.clear( );
     if (do_region_class_relabel_flag)
     {
       if (params.debug > 2)
         params.log_fs << endl << "After relabeling, nregions = " << nregions << endl;
#ifdef DEBUG
       if (params.debug > 3)
       {
         params.log_fs << endl << "After relabeling, dump of the pixel data region labels:" << endl << endl;
         for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
           if (pixel_data[pixel_index].get_region_label( ) != 0)
           {
             params.log_fs << "Element " << pixel_index << " is associated with region label ";
             pixel_data[pixel_index].print_region_label();
             params.log_fs << endl;
           }
       }
#endif
     }
     else
       if (params.debug > 2)
         params.log_fs << endl << "No relabeling required, nregions = " << nregions << endl;
     do_region_class_relabel_flag = false;

    // Initialize the region_classes incorporating optional first merge region growing
     first_merge_reg_grow(ncols,nrows,pixel_data,region_classes,nregions);
     region_classes_size = region_classes.size( );
     if (params.std_dev_wght_flag)
       for (region_index = 0; region_index < nregions; ++region_index)
         if (region_classes[region_index].get_active_flag( ))
           region_classes[region_index].calc_std_dev( );
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "After initialization in lrhseg, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < nregions; ++region_index)
         region_classes[region_index].print(region_classes);
       params.log_fs << endl << "Dump of the pixel data region labels:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
         if (pixel_data[pixel_index].get_region_label( ) != 0)
         {
           params.log_fs << "Element " << pixel_index << " is associated with region label ";
           pixel_data[pixel_index].print_region_label();
           params.log_fs << endl;
         }
     }
#endif
   } // if (recur_level == params.rnb_levels)

   double threshold = 0.0;

  // Declare arrays which will hold processing window seam information
   unsigned int index_data_size = 1;
   index_data_size = ncols*params.seam_size;
   vector<Index> row_seam_index_data(index_data_size);
   index_data_size = nrows*params.seam_size;
   vector<Index> col_seam_index_data(index_data_size);

   if (recur_level < params.rnb_levels)
   {
  // Need to set up and make a call to a deeper level of recursion
     short unsigned int recur_section = 0;
     unsigned int recur_region_index, recur_nregions;
     unsigned int recur_region_classes_size, class_label_offset = 0;
     unsigned int recur_pixel_index, recur_npixels;
     int col_offset=0, row_offset=0;
     int recur_pixel_ncols = pixel_ncols/2;
     int recur_pixel_nrows = 1;
     recur_pixel_nrows = pixel_nrows/2;
     if (pixel_ncols != ncols)
       recur_pixel_ncols = pixel_ncols;
     if (pixel_nrows != nrows)
       recur_pixel_nrows = pixel_nrows;
     recur_npixels = recur_pixel_ncols*recur_pixel_nrows;
     vector<Pixel> recur_pixel_data(recur_npixels,Pixel( ));
     vector<RegionClass> recur_region_classes(region_classes.size( ),RegionClass( ));

     nregions = 0;
     int recur_ncols = ncols/2;
     int recur_nrows = 1;
     recur_nrows = nrows/2;
     short unsigned int proc_section, next_recur_level = recur_level + 1;
     short unsigned int next_stride = stride/params.nb_strides;
     short unsigned int next_nb_sections = stride;
     for (proc_section = 0; proc_section < params.nb_strides; ++proc_section)
     {
       switch(proc_section)
       {
         case 0:  col_offset = 0; row_offset = 0;
                  recur_section = section;
                  break;
         case 1:  col_offset = recur_ncols; row_offset = 0;
                  recur_section = section + stride;
                  break;
         case 2:  col_offset = 0; row_offset = recur_nrows;
                  recur_section = section + 2*stride;
                  break;
         case 3:  col_offset = recur_ncols; row_offset = recur_nrows;
                  recur_section = section + 3*stride;
                  break;
       }
       class_label_offset = nregions;

       recur_region_classes_size = recur_region_classes.size( );
       for (recur_region_index = 0; recur_region_index < recur_region_classes_size; ++recur_region_index)
       {
         recur_region_classes[recur_region_index].clear( );
         recur_region_classes[recur_region_index].set_label(recur_region_index+1);
       }

         if (recur_level >= params.ionb_levels)
         {
            for (row = 0; row < recur_pixel_nrows; ++row)
             for (col = 0; col < recur_pixel_ncols; ++col)
             {
               pixel_index = (col+col_offset) + (row+row_offset)*pixel_ncols;
               recur_pixel_index = col + row*recur_pixel_ncols;
               recur_pixel_data[recur_pixel_index] = pixel_data[pixel_index];
             }
         }
         recur_nregions = lrhseg(class_label_offset,next_recur_level,recur_section,
                                 next_stride,next_nb_sections,recur_ncols,recur_nrows,
                                 global_nregions,threshold,spatial_data,recur_pixel_data,recur_region_classes,
                                 nghbr_heap,region_heap,temp_data);
         if (recur_level >= params.ionb_levels)
         {
            for (row = 0; row < recur_pixel_nrows; ++row)
             for (col = 0; col < recur_pixel_ncols; ++col)
             {
               pixel_index = (col+col_offset) + (row+row_offset)*pixel_ncols;
               recur_pixel_index = col + row*recur_pixel_ncols;
               pixel_data[pixel_index] = recur_pixel_data[recur_pixel_index];
             }
         }
         else
         {
           if ((next_recur_level == params.ionb_levels) && (params.nb_sections > 1))
           {
             spatial_data.save_region_class_label_map(recur_section);
             save_pixel_data(recur_section,recur_pixel_data,temp_data);
           }
         }

       if (max_threshold < threshold)
         max_threshold = threshold;

       if (((unsigned int) region_classes.size( )) < (nregions + recur_nregions))
         region_classes.resize(nregions + recur_nregions);

       region_index = nregions;
       recur_region_classes_size = recur_region_classes.size( );
       for (recur_region_index = 0; recur_region_index < recur_region_classes_size; ++recur_region_index)
       {
         if (recur_region_classes[recur_region_index].get_active_flag( ))
         {
           region_classes[region_index] = recur_region_classes[recur_region_index];
           region_label = region_index + 1;
           if (recur_region_classes[recur_region_index].get_label( ) != region_label)
           {
             if (params.debug > 0)
             {
               params.log_fs << "WARNING:  recur_region_classes.label = " << recur_region_classes[recur_region_index].get_label( );
               params.log_fs << " and region_label = " << region_label << endl;
             }
             else
             {
               cout << "WARNING:  recur_region_classes.label = " << recur_region_classes[recur_region_index].get_label( );
               cout << " and region_label = " << region_label << endl;
             }
           }
           region_classes[region_index].set_label(region_label);
           region_classes[region_index].set_merge_region_label(0);
           ++region_index;
         }
         else if (recur_region_index < recur_nregions)
         {
           if (params.debug > 0)
             params.log_fs << "WARNING:  recur_region_index " << recur_region_index << " is not active." << endl;
           else
           {
             cout << "WARNING:  recur_region_index " << recur_region_index << " is not active." << endl;
           }
         }
       }

       nregions += recur_nregions;
#ifdef DEBUG
       if ((params.debug > 1) && (nregions > 1))
       {
         params.log_fs << endl << "After processing section " << proc_section << ", dump of the region_classes:" << endl << endl;
         region_classes_size = region_classes.size( );
         for (region_index = 0; region_index < region_classes_size; ++region_index)
           if (region_classes[region_index].get_active_flag( ))
             region_classes[region_index].print(region_classes);
           else if (region_index < nregions)
             params.log_fs << endl << "WARNING: RegionClass " << (region_index+1) << " is not active." << endl;
       }
#endif
     } // for (proc_section = 0; proc_section < params.nb_strides; ++proc_section)
#ifdef DEBUG
     if (params.debug > 2)
     {
       params.log_fs << endl << "After assembling sections, dump of the region_classes:" << endl << endl;
       region_classes_size = region_classes.size( );
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         if (region_classes[region_index].get_active_flag( ))
           region_classes[region_index].print(region_classes);
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "After assembling sections, dump of the region labeling of pixel_data:" << endl << endl;
         for (row = 0; row < nrows; row++)
         {
           for (col = 0; col < ncols; col++)
           {
             pixel_index = col + row*ncols;
             pixel_data[pixel_index].print_region_label();
           }
           params.log_fs << endl;
         }
         params.log_fs << endl;
     }
#endif

     recur_region_classes.clear( );
     recur_pixel_data.clear( );
   // Add the neighborhood relationships from along the processing window seams based
   // on the *_seam_index_data.
     get_seam_index_data(recur_level,section,stride,nb_sections,
                         pixel_data,ncols,nrows,col_seam_index_data,
                         row_seam_index_data,temp_data);

     update_nghbrs_label_set(ncols,nrows,col_seam_index_data,
                             row_seam_index_data,region_classes);
#ifdef DEBUG
     if (params.debug > 2)
     {
       params.log_fs << endl << "After adding neighbors across the seams, dump of the region_classes:" << endl << endl;
       for (region_index = 0; region_index < nregions; ++region_index)
         region_classes[region_index].print(region_classes);
     }
#endif
     init_nregions = nregions;
   } // if (recur_level < params.rnb_levels)

   if (nregions > 0)
   {
  // Set up call to lhseg (HSEG) for current recursive level.
#ifdef DEBUG
    if (params.debug > 3)
    {
     params.log_fs << endl << "Dump of the pixel data:" << endl << endl;
     for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
       pixel_data[pixel_index].print(pixel_index);
    }
    if (params.debug > 3)
    {
     params.log_fs << endl << "Dump of the region data:" << endl << endl;
     region_classes_size = region_classes.size( );
     for (region_index = 0; region_index < region_classes_size; ++region_index)
       region_classes[region_index].print(region_classes);
    }
#endif
    if (params.debug > 1)
    {
      params.log_fs << endl << "At recursive level = " << recur_level;
      params.log_fs << ", calling HSEG with the number of regions = " << nregions;
      params.log_fs << endl << "and max_threshold = " << max_threshold;
      params.log_fs << endl;
    }

    unsigned int converge_nregions = params.min_nregions;
    double prev_max_threshold = max_threshold;
    max_threshold = 0.0;
    if ((recur_level == 1) && (params.chk_nregions_flag) && (params.chk_nregions > params.min_nregions))
      converge_nregions = params.chk_nregions;
    if ((recur_level == 1) && (!params.chk_nregions_flag))
      converge_nregions = UINT_MAX;
    if ((nregions > converge_nregions) ||
        ((nregions > params.conv_nregions) && (prev_max_threshold == 0.0)))
    {
     lhseg( recur_level, section, stride, nb_sections,
           converge_nregions, nregions, max_threshold, pixel_data, region_classes,
           nghbr_heap, region_heap, temp_data);

     if (params.debug > 1)
     {
       params.log_fs << endl << "After call to HSEG at recursive level = " << recur_level;
       params.log_fs << " with the number of regions = " << nregions;
       params.log_fs << endl << "and max_threshold = " << max_threshold;
#ifdef DEBUG
       if (params.debug > 3)
       {
         params.log_fs << endl << "After call to HSEG, dump of the pixel data:" << endl << endl;
         for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
         {
           pixel_data[pixel_index].print(pixel_index);
          }
       }
       if (params.debug > 2)
       {
         params.log_fs << endl << "After call to HSEG, dump of the region data:" << endl << endl;
         region_classes_size = region_classes.size( );
         for (region_index = 0; region_index < region_classes_size; ++region_index)
           if (region_classes[region_index].get_active_flag( ))
             region_classes[region_index].print(region_classes);
       }
       if (params.debug > 3)
       {
         params.log_fs << endl << "After call to HSEG, dump of the pixel_data.region_label:" << endl << endl;
          for (row = 0; row < nrows; row++)
          {
           for (col = 0; col < ncols; col++)
           {
             pixel_index = col + row*ncols;
             params.log_fs << pixel_data[pixel_index].get_region_label( ) << " ";
           }
           params.log_fs << endl;
          }
          params.log_fs << endl;
       }
#else
      params.log_fs << endl;
#endif
     }
    } // if ((nregions > converge_nregions) ||
      //     ((nregions > params.conv_nregions) && (prev_max_threshold == 0)))

    region_classes_size = region_classes.size( );
#ifdef DEBUG
    if (params.debug > 3)
    {
     params.log_fs << endl << "Before renumbering, dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
       if (region_classes[region_index].get_active_flag( ))
         region_classes[region_index].print(region_classes);
    }
#endif

  // Make the region labeling compact.
    unsigned int compact_region_index = 0;
    vector<RegionClass> compact_region_classes(nregions,RegionClass( ));
    for (region_index = 0; region_index < region_classes_size; ++region_index)
    {
      if ((region_classes[region_index].get_active_flag( )) &&
          (region_classes[region_index].get_npix( ) > 0))
      {
        compact_region_classes[compact_region_index++] = region_classes[region_index];
      }
      if (compact_region_index == nregions)
        break;
    }

 // Sort region_classes by number of pixels.
#ifndef NO_SORT
    sort(compact_region_classes.begin( ), compact_region_classes.end( ), NpixMoreThan( ));
#endif

 // Relabel region_classes as necessary to make the sorted labeling compact.
    region_relabel_pairs.clear( );
    for (region_index = 0; region_index < nregions; ++region_index)
    {
      region_classes[region_index] = compact_region_classes[region_index];
      region_label = region_classes[region_index].get_label( );
      region_classes[region_index].set_label(region_index + region_label_offset + 1);
      region_classes[region_index].clear_best_merge( );
      if (region_label != region_classes[region_index].get_label( ))
        region_relabel_pairs.insert(make_pair(region_label,region_classes[region_index].get_label( )));
    }

    if (!region_relabel_pairs.empty( ))
    {
    // Use region_relabel_pairs to renumber the nghbrs_label_set for each region.
     for (region_index = 0; region_index < nregions; ++region_index)
       region_classes[region_index].nghbrs_label_set_renumber(region_relabel_pairs);

  // Use region_relabel_pairs to renumber region_label in pixel_data.
     do_region_class_relabel(recur_level,section,stride,nb_sections,region_relabel_pairs,
                       pixel_data,temp_data);

  // region_classes is now compact.
     region_relabel_pairs.clear( );
    } // if (!region_relabel_pairs.empty( ))
    region_classes_size = region_classes.size( );
    for (region_index = nregions; region_index < region_classes_size; ++region_index)
    {
     region_classes[region_index].clear( );
     region_classes[region_index].set_label(region_index + region_label_offset + 1);
    }

    if (params.debug > 1)
    {
     params.log_fs << endl << "Completed recursive level = " << recur_level;
     params.log_fs << " in lrhseg with the number of regions = " << nregions;
     params.log_fs << endl << "and max_threshold = " << max_threshold;
#ifdef DEBUG
     if (params.debug > 2)
     {
       for (region_index = 0; region_index < nregions; ++region_index)
         if (!region_classes[region_index].get_active_flag( ))
           params.log_fs << "WARNING:  RegionClass " << region_classes[region_index].get_label( ) << " is inactive!!" << endl;
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "Exiting lrhseg, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < nregions; ++region_index)
         if (region_classes[region_index].get_active_flag( ))
           region_classes[region_index].print(region_classes,region_label_offset);
         else if (region_index < nregions)
           params.log_fs << "WARNING:  RegionClass " << region_classes[region_index].get_label( ) << " is inactive!!" << endl;
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "Exiting lrhseg, dump of the region labeling of the pixel_data:" << endl << endl;
        for (row = 0; row < nrows; row++)
        {
         for (col = 0; col < ncols; col++)
         {
           pixel_index = col + row*ncols;
           pixel_data[pixel_index].print_region_label();
         }
         params.log_fs << endl;
        }
        params.log_fs << endl;
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "Exiting lrhseg, dump of the pixel data:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
       {
         pixel_data[pixel_index].print(pixel_index);
       }
     }
#else
     params.log_fs << endl;
#endif
    }
    global_nregions -= (init_nregions - nregions);
   } // if (nregions > 0)
   oparams.percent_complete = (100*(oparams.tot_npixels-global_nregions))/oparams.tot_npixels;
   if (oparams.percent_complete == 100)
     oparams.percent_complete = 99;
   if (!params.gtkmm_flag)
     cout << "Processing is " << oparams.percent_complete << "% complete\r" << flush;

   return nregions;
 }
} // namespace HSEGTilton

