/*-----------------------------------------------------------
|
|  Routine Name: first_merge_reg_grow
|
|       Purpose: Performs randomized region oriented first merge region growing
|
|         Input: pixel_data       (Class which holds information pertaining to the data pixels)
|                ncols            (Number of columns in pixel_data)
|                nrows            (Number of rows in pixel_data)
|                
|        Output:
|
|         Other: region_classes      (Class which holds region related information)
|                nregions         (Current number of regions)
|
|       Returns: void
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt, MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: February 28, 2007.
| Modifications: March 24, 2007 - Modified to account for changes in pixel_data object.
|                May 12, 2008 - Revised to work with globally defined Params and oParams class objects
|
------------------------------------------------------------*/

#include "hseg.h"
#include "params/params.h"
#include "pixel/pixel.h"
#include "region/region_class.h"

extern HSEGTilton::Params params;

namespace HSEGTilton
{
 void first_merge_reg_grow(const int &ncols, const int& nrows,
                           vector<Pixel>& pixel_data, vector<RegionClass>& region_classes, unsigned int& nregions)
 {
   if (params.debug > 2)
   {
     if (params.init_threshold == 0.0)
       params.log_fs << "Region initialization started with nregions = " << nregions;
     else
       params.log_fs << "First merge region growing started with nregions = " << nregions;
     params.log_fs << endl;
   }

   unsigned int region_index, region_classes_size = region_classes.size();
   unsigned int pixel_index, pixel_data_size = pixel_data.size();
   unsigned int tot_npixels = 0;
   vector <unsigned int> pixel_index_vector;
   for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
   {
     if (pixel_data[pixel_index].get_mask())
     {
       pixel_index_vector.push_back(pixel_index);
       tot_npixels++;
     }
   }
   if (params.init_threshold > 0.0)
     random_shuffle(pixel_index_vector.begin(),pixel_index_vector.end());

   bool init_flag;
#ifdef CLRG
   bool merged_flag;
   unsigned int nb_nghbr, next_region_label = nregions + 1;
#endif
   unsigned int region_label;
   int col, row;
   vector<unsigned int>::const_iterator pixel_index_iter;
   pixel_index_iter = pixel_index_vector.begin();
   while (pixel_index_iter != pixel_index_vector.end())
   {
     pixel_index = *pixel_index_iter;
     row = pixel_index/ncols;     col = ((int) pixel_index) - row*((int) ncols);
     init_flag = pixel_data[pixel_index].get_init_flag();
     if (!init_flag)
     {
       pixel_data[pixel_index].set_init_flag(true);
       region_label = pixel_data[pixel_index].get_region_label();
       if (region_label == 0)
       {
         nregions++;
#ifdef CLRG
         region_label = next_region_label;
#else
         region_label = nregions;
#endif
         pixel_data[pixel_index].set_region_label(region_label);
#ifdef CLRG
         if (region_classes_size < region_label)
         {
           region_classes_size = region_label;
#else
         if (region_classes_size <= nregions)
         {
           region_classes_size = nregions + 1;
#endif
           if (region_classes_size > 1000)
             region_classes_size += 1000;
           else if (region_classes_size < 10)
             region_classes_size += 10;
           else
             region_classes_size *= 2;
           region_classes.resize(region_classes_size);
         }
         region_index = region_label - 1;
         region_classes[region_index].clear();
         region_classes[region_index].set_label(region_label);
         region_classes[region_index].fm_init(pixel_data, region_classes,
                                           col, row, ncols, nrows);
         if (params.std_dev_wght_flag)
           region_classes[region_index].calc_std_dev();

#ifdef CLRG
         nb_nghbr = region_classes[region_index].find_best_merge(region_classes);
         merged_flag = false;
         if (nb_nghbr > 1)
         {
           merged_flag = region_classes[region_index].merge_best2_nghbrs(pixel_data, region_classes,
                                                                      col, row, ncols, nrows);
           if (merged_flag)
             nregions--;
         }
         if ((region_classes[region_index].get_best_nghbr_dissim() <= params.init_threshold) || (merged_flag))
         {
           region_label = region_classes[region_index].do_best_merge(region_classes);
           pixel_data[pixel_index].set_region_label(region_label);
           nregions--;
         }
         next_region_label++;
#else // CLRG
         while (region_classes[region_index].find_merge(pixel_data))
         {
           region_classes[region_index].do_merge(ncols,nrows,nregions,
                                              pixel_data,region_classes);
         }
#endif // !CLRG
       }
       else
       {
#ifdef CLRG
         if (region_classes_size < region_label)
         {
           region_classes_size = region_label;
           if (region_classes_size > 1000)
             region_classes_size += 1000;
           else if (region_classes_size < 10)
             region_classes_size += 10;
           else
             region_classes_size *= 2;
           region_classes.resize(region_classes_size);
         }
#endif // CLRG
         region_index = region_label - 1;
         region_classes[region_index].fm_init(pixel_data, region_classes,
                                           col, row, ncols, nrows);
         if (params.std_dev_wght_flag)
           region_classes[region_index].calc_std_dev();
       }
     } // if (!init_flag)
     pixel_index_iter++;
   }
   pixel_index_iter = pixel_index_vector.begin();
   while (pixel_index_iter != pixel_index_vector.end())
   {
     pixel_index = *pixel_index_iter;
     pixel_data[pixel_index].set_init_flag(false);
     pixel_index_iter++;
   }

#ifdef CLRG
  // Update pixel_data.region_label based on merges performed.
   unsigned int merge_region_index, merge_region_label;
   for (region_index = 0; region_index < region_classes_size; ++region_index)
   {
     merge_region_index = region_index;
     merge_region_label = region_classes[merge_region_index].get_merge_region_label();
     if (merge_region_label != 0)
     {
       while (merge_region_label != 0)
       {
         merge_region_index = merge_region_label - 1;
         merge_region_label = region_classes[merge_region_index].get_merge_region_label();
       }
       region_classes[region_index].set_merge_region_label(region_classes[merge_region_index].get_label());
     }
   }

   map<unsigned int,unsigned int> region_class_relabel_pairs;
   for (region_index = 0; region_index < region_classes_size; ++region_index)
   {
     region_label = region_index + 1;
     merge_region_label = region_classes[region_index].get_merge_region_label();
     if (merge_region_label != 0)
        region_class_relabel_pairs.insert(make_pair(region_label,merge_region_label));
   }
   if (!(region_class_relabel_pairs.empty()))
     do_region_class_relabel(region_class_relabel_pairs,pixel_data);
   region_class_relabel_pairs.clear();
   for (region_index = 0; region_index < region_classes_size; ++region_index)
     region_classes[region_index].set_merge_region_label(0);
#endif

// Compact the region_classes vector, sort by size, and erase unneeded elements
   unsigned int compact_region_index = 0;
   vector<RegionClass> compact_region_classes(nregions,RegionClass());
   for (region_index = 0; region_index < region_classes_size; ++region_index)
   {
     if (region_classes[region_index].get_active_flag())
     {
       region_classes[region_index].nghbrs_label_set_clear();
       compact_region_classes[compact_region_index++] = region_classes[region_index];
     }
     if (compact_region_index == nregions)
       break;
   }
   if (nregions > params.max_nregions)
     region_classes_size = nregions;
   else
     region_classes_size = params.max_nregions;
   if (region_classes.size() != region_classes_size)
     region_classes.resize(region_classes_size);

 // Sort region_classes by number of pixels.
#ifndef NO_SORT
    sort(compact_region_classes.begin(), compact_region_classes.end(), NpixMoreThan());
#endif

 // Renumber region_classes, set up region_class_relabel_pairs.
#ifndef CLRG
   map<unsigned int,unsigned int> region_class_relabel_pairs;
#endif
   for (region_index = 0; region_index < nregions; ++region_index)
   {
     region_classes[region_index] = compact_region_classes[region_index];
     region_label = region_classes[region_index].get_label();
     region_classes[region_index].set_label(region_index+1);
     region_classes[region_index].clear_best_merge();
     if (region_label != region_classes[region_index].get_label())
       region_class_relabel_pairs.insert(make_pair(region_label,region_classes[region_index].get_label()));
   }

   if (!(region_class_relabel_pairs.empty()))
   {
   // Use region_class_relabel_pairs to renumber region_label in pixel_data.
     do_region_class_relabel(region_class_relabel_pairs,pixel_data);
   }

 // Initialize region_classes.nghbrs_label_set from pixel_data
   for (region_index = 0; region_index < nregions; ++region_index)
     region_classes[region_index].nghbrs_label_set_clear();
   nghbrs_label_set_init(ncols,nrows,
                         pixel_data,region_classes);

#ifdef DEBUG
   if (params.debug > 3)
   {
     params.log_fs << endl << "After initialization in first_merge_reg_grow, dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < nregions; ++region_index)
       region_classes[region_index].print(region_classes);
     params.log_fs << endl << "Dump of the pixel data region labels:" << endl << endl;
     for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
       if (pixel_data[pixel_index].get_region_label() != 0)
       {
         params.log_fs << "Element " << pixel_index << " is associated with region label ";
         pixel_data[pixel_index].print_region_label();
         params.log_fs << endl;
       }
   }
#endif

   if (params.debug > 1)
   {
     if (params.init_threshold > 0.0)
     {
       params.log_fs << "Region initialization completed utilizing first merge region growing";
       params.log_fs << ", with the number of regions = " << nregions << endl;
     }
     else
       params.log_fs << "Region initialization completed, with the number of regions = " << nregions << endl;
   }

   return;
 }
} // namespace HSEGTilton
