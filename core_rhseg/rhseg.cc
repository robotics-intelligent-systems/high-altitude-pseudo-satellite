/*-----------------------------------------------------------
|
|  Routine Name: rhseg - Recursive version of Hierarchical Segmentation
|
|       Purpose: Main function for the recursive version of HSEG
|
|         Input: spatial_data     (Class which holds information pertaining to input and output spatial data)
|                pixel_data       (Class which holds information pertaining to the pixels processed by this task)
|                
|        Output: region_classes      (Class which holds region related information)
|                max_threshold    (Maximum merging threshold encountered)
|
|         Other: temp_data        (buffers used in communications between parallel tasks)
|
|       Returns: nregions         (Current number of regions)
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt, MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: December 9, 2002.
| Modifications: February 7, 2003 - Changed region_index to region_label
|                May 20, 2003 - Added option to initialize with Classical Region Growing.
|                September 24, 2003 - Eliminated use of the index_class class (index_data).
|                May 29, 2005 - Added temporary file I/O for faster processing of large data sets
|                October 14, 2005 - Added slice dimension (extension to three-dimensional analysis)
|                May 12, 2008 - Revised to work with globally defined Params and oParams class objects
|                September 5, 2008 - Modified rhseg call vis-a-vis nregions & max_threshold
|
------------------------------------------------------------*/

#include "hseg.h"
#include "params/params.h"
#include "region/region_class.h"
#include "spatial/spatial.h"
#include "pixel/pixel.h"

extern HSEGTilton::Params params;
extern HSEGTilton::oParams oparams;

namespace HSEGTilton
{
 unsigned int rhseg(unsigned int& nregions, double& max_threshold, Spatial& spatial_data, 
                    vector<Pixel>& pixel_data, vector<RegionClass>& region_classes, 
                    vector<RegionClass *>& nghbr_heap, vector<RegionClass *>& region_heap, Temp& temp_data)
 {
   short unsigned int section = 0;

   int ncols = params.padded_ncols;
   int nrows = params.padded_nrows;

   unsigned int global_nregions = nregions;
   nregions = lrhseg(0,params.recur_level,section,params.stride,params.nb_sections,
                     ncols,nrows,global_nregions,max_threshold,spatial_data,pixel_data,region_classes,
                     nghbr_heap,region_heap,temp_data);

#ifdef DEBUG
   unsigned int pixel_index, pixel_data_size = pixel_data.size( );
   if (params.debug > 3)
   {
     params.log_fs << endl << "After calls to lrhseg in hseg, dump of the pixel data:" << endl << endl;
     for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
     {
       pixel_data[pixel_index].print(pixel_index);
     }
   }
   unsigned int region_index, region_classes_size = region_classes.size( );
   if (params.debug > 3)
   {
     params.log_fs << endl << "After calls to lrhseg in hseg, dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
       if (region_classes[region_index].get_active_flag( ))
         region_classes[region_index].print(region_classes);
   }
#endif
   return nregions;
 }
} // namespace HSEGTilton

