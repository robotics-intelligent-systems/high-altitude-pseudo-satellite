/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<<
   >>>>
   >>>>       Purpose: Common include file for the main program.
   >>>>                Used by the core rhseg program for compiler directives
   >>>>                and constants (through defines)
   >>>>
   >>>>    Written By: James C. Tilton, NASA GSFC, Mail Code 606.3, Greenbelt, MD 20771
   >>>>                James.C.Tilton@nasa.gov
   >>>>
   >>>>       Written: November 9, 2009 (Base on rhseg version 1.47)
   >>>> Modifications: 
   >>>>
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<<<< */

//* Compiler Directives: Leaving defined or undefining selects particular compiler options
//* Notes:
//* ->Normally MSE_SQRT should be left defined.
//* ->Normally CLRG, NO_SORT, DEBUG and TIME_IT should be undefined.
//* ->Other flags should be left defined or made undefined depending on the situation.
#ifndef DEFINES_H
#define DEFINES_H

#define WINDOWS
#undef WINDOWS          // Leave defined if compiling on a Windows machine (except for cygwin)
#define CYGWIN
#undef CYGWIN          // Leave defined if compiling under cygwin on a Windows machine
#define ARTEMIS
#undef ARTEMIS       // Leave defined if compiling for the Bartron "artemis" cluster
#define MSE_SQRT
//#undef MSE_SQRT      // Leave defined if using the square root of the MSE dissimilarity function.
#define CLRG
#undef CLRG          // Leave defined if CLRG first merge region growing is desired.
                     // Otherwise, MARG first merge region growing is utilized.
#define NO_SORT
#undef NO_SORT       // Undefine if you want to sort the regions by npix or distance to minimum vector
#define DEBUG
#undef DEBUG         // Leave defined if debug outputs are desired in log file.
#define TIME_IT
#undef TIME_IT       // Leave defined if detailed timings are desired in log file.

//* Constant Definitions
#define MAX_NREGIONS         4000 // Used to determine default number of recursive levels
#define MAX_NPIXELS        262144 // Maximum number of pixels in processing window at the lowest recursive level
                                  // at which data I/O is performed
#define MAXMDIR                26 // Maximum number of neighbors allowed
#define TIME_SIZE              26 // Dimension size for vector used in timing buffer string
#define PRINT_INTERVAL      10000
#define MAX_TEMP_SIZE          44
#define NGHBRS_LABEL_SET_SIZE_LIMIT  200

// Used in rhseg_read (HSEGReader)
#define MIN_STD_DEV_NPIX        10

// Used in rhseg_view (HSEGViewer)
#define NUMBER_OF_LABELS        69
#define HIGHLIGHT_COLOR         67   // NUMBER_OF_LABELS - 2
#define BAD_DATA_QUALITY_LABEL  68   // NUMBER_OF_LABELS - 1
#define INIT_SEG_LEVEL           0

// For Image Class
#define MAX_IO_SIZE      402653184 // 8192*8192*6
#define MAX_COLORMAP_SIZE SHRT_MAX

// For DisplayImage Class
#define MAX_LINKED_IMAGES       12
#define MAX_VISIBLE_SIZE       512
#define MAX_THUMB_SIZE         128

// Program defaults
#define INPUT_IMAGE_FILE "_input"
#define MASK_FILE "_mask"
#define MASK_VALUE 0
#define REGION_MAP_IN_FILE "_region_map_in"
#define CLASS_LABELS_MAP_FILE "_class_labels_map"
#define BOUNDARY_MAP "_boundary_map"
#define BOUNDARY_MAP_FLAG false
#define REGION_CLASSES "_region_classes"
#define OBJECT_LABELS_MAP "_object_labels_map"
#define REGION_OBJECTS "_region_objects"
#define REGION_OBJECTS_FLAG false
#define REGION_SUM_FLAG_MAX 20
#define REGION_STD_DEV_FLAG false
#define REGION_BOUNDARY_NPIX_FLAG false
#define REGION_THRESHOLD_FLAG false
#define REGION_NGHBRS_LIST_FLAG true // Always true
#define RHSEG_SCALE 1.0
#define RHSEG_OFFSET 0.0
#define DISSIM_CRIT 6
#define CHK_NREGIONS 64
#define CHK_NREGIONS_FLAG true
#define HSEG_OUT_NREGIONS_FLAG false
#define HSEG_OUT_THRESHOLDS_FLAG false
#define CONV_NREGIONS 2
#define GDISSIM_FLAG false

#define DEBUG_DEFAULT_VALUE 1
#define NORMIND 2
#define INIT_THRESHOLD 0.0
#define STD_DEV_WGHT 0.0

#define MIN_NPIXELS 200

#endif // DEFINES_H
