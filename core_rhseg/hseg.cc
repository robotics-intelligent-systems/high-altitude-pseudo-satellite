/*-----------------------------------------------------------
|
|  Routine Name: hseg - Hierarchical Segmentation
|
|       Purpose: Main function for the Hierarchical Segmentation (HSEG) program
|
|         Input: 
|
|        Output: 
|
|       Returns: TRUE (1) on success, FALSE (0) on failure
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt, MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: December 9, 2002.
| Modifications: February 25, 2003 - Modified output to output parameter file (-oparam)
|                August 20, 2003 - Reorganized program and added user controlled byteswapping
|                September 24, 2003 - Eliminated use of the index_class class (index_data)
|                October 18, 2003 - Added boundary_npix_file option
|                October 31, 2003 - Added subrlblmap_file and subregmerges_file options
|                October 31, 2003 - Eliminated use of final_region_list
|                November 5, 2003 - Modified Params structure and made class member variables all private
|                November 26, 2003 - Added output paramater file for connected regions (-conn_oparam)
|                March 2, 2004 - Added boundary_map option
|                May 17, 2004 - Modified calculation of critval and added global_dissim
|                January 4, 2004 - Changed from sorting lists to utilizing a heap structure
|                May 9, 2005 - Added code for integration with VisiQuest
|                May 29, 2005 - Added temporary file I/O for faster processing of large data sets
|                August 9, 2005 - Added "conv_criterion" and "gstd_dev_crit_flag" parameters
|                October 14, 2005 - Added slice dimension (extension to three-dimensional analysis)
|                December 23, 2005 - Changed sorting for region labeling to be by distance from minimum vector
|                November 1, 2007 - Revised method for selecting iterations at which segmentations are output,
|                                   eliminating the "conv_criterion" and "conv_factor" parameters.
|                                   This revision also eliminated the "gdissim_crit" and "gstd_dev_crit"
|                                   parameters and added the "gdissim_flag" parameter
|                November 1, 2007 - Removed VisiQuest related code
|                November 9, 2007 - Combined region feature output into region_object list file
|                May 12, 2008 - Revised to work with globally defined Params and oParams class objects
|                August 8, 2008 - Added hseg_out_thresholds and hseg_out_nregions parameters.
|                July 30, 2009 - Added inclusion of C++ algorithm library <algorithm>
|
------------------------------------------------------------*/

#include "hseg.h"
#include "params/params.h"
#include "spatial/spatial.h"
#include "pixel/pixel.h"
#include "region/region_class.h"
#include "region/region_object.h"
#include "results/results.h"
#include <iostream>
#include <cmath>
#include <algorithm>
#ifdef DEBUG
#include <ctime>
#endif
#ifdef GTKMM
#include <pthread.h>
#endif

extern HSEGTilton::Params params;
extern HSEGTilton::oParams oparams;

namespace HSEGTilton
{
#ifdef GTKMM
 void *hseg_thread(void *threadid)
 {
   params.gtkmm_flag = hseg();
   oparams.percent_complete = 100;
   pthread_exit(NULL);
   return threadid;
 }
#endif
 bool hseg()
 {
// Declaration of temp_data structure and elements - used for both data I/O
// to and from temporary data files (serial) and for data transfer between
// processing tasks (parallel).
   Temp temp_data;
   temp_data.byte_buf_size = 0;
   temp_data.byte_buffer = NULL;
   temp_data.short_buf_size = 0;
   temp_data.short_buffer = NULL;
   temp_data.int_buf_size = 0;
   temp_data.int_buffer = NULL;
   temp_data.float_buf_size = 0;
   temp_data.float_buffer = NULL;
   temp_data.double_buf_size = 0;
   temp_data.double_buffer = NULL;

// Declare spatial (image) data
   Spatial spatial_data;

// Declare pixel_data.
   Pixel::set_static_vals();
   unsigned int npixels = params.pixel_ncols*params.pixel_nrows;
   vector<Pixel> pixel_data(npixels,Pixel());

// Obtain input data
   spatial_data.read_data(pixel_data,temp_data);

// Compute the scale and offset parameters
   oparams.tot_npixels = scale_offset(pixel_data,temp_data);
   unsigned int nregions = oparams.tot_npixels;

// Declare region_classes.
   RegionClass::set_static_vals();
   vector<RegionClass> region_classes(params.max_nregions,RegionClass());
   unsigned int region_index;
   unsigned int region_classes_size = params.max_nregions;
   for (region_index = 0; region_index < region_classes_size; ++region_index)
     region_classes[region_index].set_label(region_index+1);

#ifdef DEBUG
   time_t now;
   if (params.debug > 1)
   {
     now = time(NULL);
     params.log_fs << "Calling RHSEG at " << ctime(&now) << endl;
   }
#endif

   double max_threshold = 0.0;
   vector<RegionClass *> nghbr_heap(1);
   vector<RegionClass *> region_heap(1);
   nregions = rhseg(nregions,max_threshold,spatial_data,pixel_data,region_classes,
                    nghbr_heap,region_heap,temp_data);

// NOTE: Region labeling is guaranteed compact exiting rhseg.
   if (params.debug > 2)
   {
     params.log_fs << "Returned to main HSEG function with the number of regions = " << nregions << endl;
     params.log_fs << "and maximum merging threshold = " << max_threshold;
#ifdef DEBUG
     now = time(NULL);
     params.log_fs << " at " << ctime(&now) << endl;
#else
     params.log_fs << endl;
#endif
   }
   region_classes_size = nregions;
   region_classes.resize(region_classes_size);

#ifdef DEBUG
   if (params.debug > 3)
   {
     params.log_fs << endl << "Dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < nregions; ++region_index)
       region_classes[region_index].print(region_classes);
   }
#endif

   short unsigned int section = 0;
   unsigned int converge_nregions = UINT_MAX;
   unsigned int region_label;
   map<unsigned int,unsigned int> region_relabel_pairs;
   double threshold;
   bool process_flag;
   if (params.chk_nregions_flag)
   {
     process_flag = (nregions > params.chk_nregions);
     converge_nregions = params.chk_nregions;
   }
   else if (params.hseg_out_nregions_flag)
   {
     process_flag = (nregions > params.hseg_out_nregions[0]);
     converge_nregions = params.hseg_out_nregions[0];
   }
   else // if (params.hseg_out_thresholds_flag)
   {
     process_flag = (max_threshold < params.hseg_out_thresholds[0]);
     converge_nregions = params.conv_nregions;
   }
   process_flag = process_flag || ((nregions > params.conv_nregions) && (max_threshold == 0.0));
   if (process_flag)
   {
     if (params.debug > 1)
     {
       params.log_fs << endl << "Calling HSEG with the number of regions = " << nregions << endl;
       params.log_fs << "and maximum merging threshold = " << max_threshold;
#ifdef DEBUG
       now = time(NULL);
       params.log_fs << " at " << ctime(&now) << endl;
#else
       params.log_fs << endl;
#endif
     }
     if ((params.spclust_start == nregions) && (params.debug > 0))
     {
       params.log_fs << "Spectral clustering started at " << nregions << " regions";
       params.log_fs << " and maximum merging threshold = " << max_threshold << endl;
     }

     threshold = max_threshold;
     lhseg(params.recur_level, section, params.stride, params.nb_sections,
           converge_nregions, nregions, threshold, pixel_data, region_classes,
           nghbr_heap, region_heap, temp_data);
     if (params.debug > 2)
     {
       params.log_fs << "Exited HSEG with the number of regions = " << nregions;
#ifdef DEBUG
       now = time(NULL);
       params.log_fs << " at " << ctime(&now) << endl;
#else
       params.log_fs << endl;
#endif
     }
     region_classes_size = region_classes.size();
     if (threshold > max_threshold)
       max_threshold = threshold;
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "Exiting lhseg, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         if (region_classes[region_index].get_active_flag())
           region_classes[region_index].print(region_classes);
     }
#endif
  // Sort region_classes by distance from minimum vector and erase unneeded elements
     unsigned int compact_region_index = 0;
     vector<RegionClass> compact_region_classes(nregions,RegionClass());
     for (region_index = 0; region_index < region_classes_size; ++region_index)
     {
       if (region_classes[region_index].get_active_flag())
       {
         region_classes[region_index].set_min_region_dissim();
         compact_region_classes[compact_region_index++] = region_classes[region_index];
       }
       if (compact_region_index == nregions)
         break;
     }
     region_classes_size = nregions;
     region_classes.resize(region_classes_size);

#ifndef NO_SORT
     sort(compact_region_classes.begin(), compact_region_classes.end(), DistToMin());
#endif
   // Renumber region_classes from darkest region to brightest, set up region_relabel_pairs.
     for (region_index = 0; region_index < nregions; ++region_index)
     {
       region_classes[region_index] = compact_region_classes[region_index];
       region_label = region_classes[region_index].get_label();
       region_classes[region_index].set_label(region_index+1);
       if (region_label != region_classes[region_index].get_label())
         region_relabel_pairs.insert(make_pair(region_label,region_classes[region_index].get_label()));
     }

     if (!(region_relabel_pairs.empty()))
     {
     // Use region_relabel_pairs to renumber the nghbrs_label_set for each region.
       for (region_index = 0; region_index < nregions; ++region_index)
         region_classes[region_index].nghbrs_label_set_renumber(region_relabel_pairs);

     // Use region_relabel_pairs to renumber region_label in pixel_data.
       do_region_class_relabel(params.recur_level,section,params.stride,params.nb_sections,
                         region_relabel_pairs,pixel_data,temp_data);

     }
   } // if ((nregions > converge_nregions) ||
     //     ((nregions > params.conv_nregions) && (max_threshold == 0.0)))

  // NOTE:  region labeling is compact at this point
   unsigned int nghbr_heap_size = nregions+1;
   unsigned int region_heap_size = 1;
   if (params.spclust_wght_flag)
     region_heap_size += nregions;
   nghbr_heap.resize(nghbr_heap_size);
   region_heap.resize(region_heap_size);
   for (region_index = 0; region_index < nregions; ++region_index)
   {
     region_classes[region_index].clear_best_merge();
     if (params.region_std_dev_flag)
       region_classes[region_index].calc_std_dev();
     nghbr_heap[region_index] = &region_classes[region_index];
     if (params.spclust_wght_flag)
     {
       region_heap[region_index] = &region_classes[region_index];
     }
   }
   nghbr_heap[--nghbr_heap_size] = NULL;
   region_heap[--region_heap_size] = NULL;

   unsigned int heap_index;
   for (heap_index = 0; heap_index < nghbr_heap_size; ++heap_index)
     nghbr_heap[heap_index]->best_nghbr_init(region_classes);
   make_nghbr_heap(nghbr_heap, nghbr_heap_size);

   if (region_heap_size > 0)
   {
     for (heap_index = 0; heap_index < region_heap_size; ++heap_index)
       region_heap[heap_index]->best_region_init(heap_index,region_heap,region_heap_size);
     make_region_heap(region_heap, region_heap_size);
   }
#ifdef DEBUG
   if (params.debug > 3)
   {
     params.log_fs << endl << "After creation of final region_classes, dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < nregions; ++region_index)
       region_classes[region_index].print(region_classes);
   }
   unsigned int pixel_index, pixel_data_size = pixel_data.size();
   if (params.debug > 3)
   {
     params.log_fs << endl << "After creation of final region_classes, dump of the pixel data:" << endl << endl;
     for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
     {
       pixel_data[pixel_index].print(pixel_index);
     }
   }
   if (params.debug > 3)
   {
     params.log_fs << endl << "After creation of final region_classes, dump of the pixel data.region_label:" << endl << endl;
      for (int row = 0; row < params.padded_nrows; row++)
      {
       for (int col = 0; col < params.padded_ncols; col++)
       {
         pixel_index = col + row*params.padded_ncols;
         pixel_data[pixel_index].print_region_label();
       }
       params.log_fs << endl;
      }
      params.log_fs << endl;
   }
#endif

   double global_dissim = 0.0, numpix = 0.0;
   double region_numpix;
   if (params.gdissim_flag)
   {
     if ((params.dissim_crit == 1) || (params.dissim_crit == 2) || (params.dissim_crit == 3) ||
         (params.dissim_crit == 4) || (params.dissim_crit == 5) || (params.dissim_crit == 8) ||
         (params.dissim_crit == 10))
     {
       for (region_index = 0; region_index < nregions; ++region_index)
         region_classes[region_index].set_sum_pixel_gdissim(0.0);
       update_sum_pixel_gdissim(params.recur_level,section,params.stride,params.nb_sections,
                                pixel_data,region_classes,temp_data);
     }

     for (region_index = 0; region_index < nregions; ++region_index)
     {
       if ((params.dissim_crit == 6) || (params.dissim_crit == 7) || (params.dissim_crit == 9))
         region_classes[region_index].calc_sum_pixel_gdissim();
       region_numpix = region_classes[region_index].get_npix();
       numpix += region_numpix;
       global_dissim += region_classes[region_index].get_sum_pixel_gdissim();
#ifdef DEBUG
       if (params.debug > 3)
       {
         params.log_fs << "For region label " << region_classes[region_index].get_label();
         params.log_fs << ", sum_pixel_gdissim = ";
         params.log_fs << region_classes[region_index].get_sum_pixel_gdissim();
         params.log_fs << " and npix = " << region_classes[region_index].get_npix() << endl;
       }
#endif
     }
     if ((params.dissim_crit == 6) || (params.dissim_crit == 7))
     {
       global_dissim /= (numpix-1.0);
#ifdef MSE_SQRT
       global_dissim = sqrt(global_dissim); // Added to make dimensionality consistent
#endif
     }
     else
       global_dissim /= numpix;
   }

   short unsigned int hlevel=0;
   Results results_data;
   RegionObject::set_static_vals();
   unsigned int region_objects_size=0, region_object_index, region_object_label;
   unsigned int nb_objects=0;
   vector<RegionObject> region_objects(nregions,RegionObject());
   map<unsigned int,unsigned int> region_object_relabel_pairs;
   spatial_data.update_region_label_map(params.recur_level,section,params.stride,
                                        params.nb_sections,pixel_data,
                                        temp_data);
   if (params.region_nb_objects_flag)
   {
     connected_component_init(params.recur_level,section,params.stride,params.nb_sections,
                              params.padded_ncols,params.padded_nrows,
                              nb_objects,region_objects,spatial_data,temp_data);

     nb_objects = do_region_objects_init(params.recur_level,section,params.stride,
                                              params.nb_sections,params.padded_ncols,params.padded_nrows,
                                              pixel_data,spatial_data,region_objects,
                                              temp_data);
     region_objects_size = region_objects.size();
     if ((params.std_dev_wght_flag) || (params.region_std_dev_flag))
       for (region_object_index = 0; region_object_index < region_objects_size; ++region_object_index)
         if (region_objects[region_object_index].get_active_flag())
           region_objects[region_object_index].calc_std_dev();
#ifndef NO_SORT
   // Set min_region_dissim
     for (region_object_index = 0; region_object_index < region_objects_size; ++region_object_index)
       if (region_objects[region_object_index].get_active_flag())
         region_objects[region_object_index].set_min_region_dissim();
   // Sort region_objects by distance from minimum vector
     sort(region_objects.begin(), region_objects.end(), ObjectDistToMin());
   // Renumber region_objects from darkest region to brightest, set up region_object_relabel_pairs.
     region_object_relabel_pairs.clear();
     for (region_object_index = 0; region_object_index < region_objects_size; ++region_object_index)
     {
       region_object_label = region_objects[region_object_index].get_label();
       region_object_relabel_pairs.insert(make_pair(region_object_label,(region_object_index+1)));
       region_object_label = region_object_index + 1;
       region_objects[region_object_index].set_label(region_object_label);
       region_objects[region_object_index].set_merge_region_label(region_object_label);
       region_objects[region_object_index].clear_region_object_info();
     }
   // Use do_region_object_relabel_pairs to renumber region_object_label_map in spatial_data.
     if (!(region_object_relabel_pairs.empty()))
       do_region_object_relabel(params.recur_level,section,params.stride,params.nb_sections,
                              region_object_relabel_pairs,spatial_data,
                              params.padded_ncols,params.padded_nrows,
                              temp_data);
#endif

     for (region_index = 0; region_index < nregions; ++region_index)
       region_classes[region_index].clear_region_info();
     update_region_object_info(params.recur_level,section,params.stride,params.nb_sections,
                             hlevel,spatial_data,region_objects,temp_data);
     update_region_class_info(params.recur_level,section,params.stride,params.nb_sections,
                        hlevel,spatial_data,region_classes,temp_data);
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "After call to update_region_class_info, dump of the pixel data:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
       {
         pixel_data[pixel_index].print(pixel_index);
       }
     }
#endif
   }
   else if ((params.region_boundary_npix_flag) || (params.boundary_map_flag))
   {
     for (region_index = 0; region_index < nregions; ++region_index)
       region_classes[region_index].clear_region_info();
     boundary_map(params.recur_level,section,params.stride,params.nb_sections,
                  params.padded_ncols,params.padded_nrows,
                  0,spatial_data,temp_data);
     update_region_class_info(params.recur_level,section,params.stride,params.nb_sections,
                        hlevel,spatial_data,region_classes,temp_data);
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "After call to update_region_class_info, dump of the pixel data:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
       {
         pixel_data[pixel_index].print(pixel_index);
       }
     }
#endif
   }
   if (params.debug > 2)
   {
     params.log_fs << endl << "Initiating hierarchical segmentation output at nregions = " << nregions;
     params.log_fs << ", with maximum merging threshold = " << max_threshold << endl;
     if (params.gdissim_flag)
       params.log_fs << " with global dissimilarity value = " << global_dissim << endl;
   }


  // Open output files
   results_data.set_buffer_sizes(nregions,nb_objects);
   results_data.open_output();
   spatial_data.open_region_label_map_output();
   if (params.debug > 0)
   {
     params.log_fs << endl << "Data written at hlevel = " << hlevel << " for " << nregions << " region classes,";
     params.log_fs << " with maximum merging threshold = " << max_threshold << endl;
     if (params.gdissim_flag)
       params.log_fs << " and with global dissimilarity value = " << global_dissim << endl;
     if (params.region_nb_objects_flag)
       params.log_fs << "There are " << nb_objects << " region objects" << endl;
     params.log_fs << endl;
   }
   else
   {
     cout << endl << "Data written at hlevel = " << hlevel << " for " << nregions << " region classes,";
     cout << " with maximum merging threshold = " << max_threshold << endl;
     if (params.gdissim_flag)
       cout << " and with global dissimilarity value = " << global_dissim << endl;
     if (params.region_nb_objects_flag)
       cout << "There are " << nb_objects << " region objects" << endl;
     cout << endl;
   }
  // Write data.
   results_data.write(hlevel,nregions,nb_objects,region_classes,region_objects);
   oparams.int_buffer_size.push_back(results_data.get_int_buffer_index());
   oparams.max_threshold.push_back(max_threshold);
   if (params.gdissim_flag)
     oparams.gdissim.push_back(global_dissim);
   spatial_data.write_region_label_map(temp_data);
   spatial_data.close_region_label_map_output();
#ifdef DEBUG
   if (params.debug > 1)
   {
     params.log_fs << endl << "After writing out results, dump of the region class data:" << endl << endl;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
       if (region_classes[region_index].get_active_flag())
         region_classes[region_index].print(region_classes);
     if (params.region_objects_list_flag)
     {
       params.log_fs << endl << "After writing out results, dump of the region object data:" << endl << endl;
       for (region_index = 0; region_index < region_objects_size; ++region_index)
         if (region_objects[region_index].get_active_flag())
           region_objects[region_index].print(region_objects);
     }
     params.log_fs << endl << "After writing out results, dump of region classes map:" << endl << endl;
     spatial_data.print_class_labels_map(hlevel,region_classes);
     if (params.object_labels_map_flag)
     {
       params.log_fs << endl << "After writing out results, dump of region objects map:" << endl << endl;
       spatial_data.print_object_labels_map(hlevel,region_objects);
     }
   }
   if (params.debug > 3)
   {
     params.log_fs << endl << "After call to write_region_label_map, dump of the pixel data:" << endl << endl;
     for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
     {
       pixel_data[pixel_index].print(pixel_index);
     }
   }
#endif
   hlevel++;

// Perform region growing and spectral clustering with selected hierarchical segmentation output.
   converge_nregions = params.conv_nregions;
   unsigned int prev_nregions = nregions;
   region_classes_size = region_classes.size();

   bool hseg_out_flag;
   for (region_index = 0; region_index < region_classes_size; ++region_index)
     region_classes[region_index].reset_merged_flag();
   while (nregions > converge_nregions)
   {
     if (params.chk_nregions_flag)
     {
       hseg_out_flag = merge_regions(true, converge_nregions, nghbr_heap, nghbr_heap_size, 
                                     region_heap, region_heap_size, region_classes, nregions, max_threshold);
     }
     else
     {
       merge_regions(false, converge_nregions, nghbr_heap, nghbr_heap_size, 
                     region_heap, region_heap_size, region_classes, nregions, max_threshold);
       hseg_out_flag = false;
       if (params.hseg_out_nregions_flag)
       {
         if (hlevel < params.nb_hseg_out_nregions)
           hseg_out_flag = (nregions <= params.hseg_out_nregions[hlevel]);
         else
           hseg_out_flag = (nregions <= converge_nregions);
       }
       else // if (params.hseg_out_thresholds_flag)
       {
         if (hlevel < params.nb_hseg_out_thresholds)
           hseg_out_flag = (max_threshold >= params.hseg_out_thresholds[hlevel]);
         else
           hseg_out_flag = (nregions <= converge_nregions);
       }
     }

#ifdef DEBUG
     if (params.debug > 0)
     {
       params.log_fs << endl << "After call to merge_regions, max_threshold = " << max_threshold << endl;
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "After call to merge_regions, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         if (region_classes[region_index].get_active_flag())
           region_classes[region_index].print(region_classes);
     }
     if (params.debug > 3)
     {
       params.log_fs << endl << "After call to merge_regions, dump of the pixel data:" << endl << endl;
       for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
       {
         pixel_data[pixel_index].print(pixel_index);
       }
     }
#endif

     if ((hseg_out_flag) || (nregions <= converge_nregions))
     {
// Update pixel_data.region_label
       unsigned int merge_region_index, next_merge_region_label;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         if (!(region_classes[region_index].get_active_flag()))
         {
           merge_region_index = region_index;
           next_merge_region_label = region_classes[merge_region_index].get_merge_region_label();
           while (next_merge_region_label != 0)
           {
             merge_region_index = next_merge_region_label - 1;
             next_merge_region_label = region_classes[merge_region_index].get_merge_region_label();
           }
           region_classes[region_index].set_merge_region_label(region_classes[merge_region_index].get_label());
         }
       region_relabel_pairs.clear();
       for (region_index = 0; region_index < region_classes_size; ++region_index)
       {
         region_label = region_classes[region_index].get_label();
         if ((!region_classes[region_index].get_active_flag()) && (region_classes[region_index].get_merged_flag()))
         {
           region_relabel_pairs.insert(make_pair(region_label,
                                       region_classes[region_classes[region_index].get_merge_region_label() - 1].get_label()));
         }
       } 
       if (!(region_relabel_pairs.empty()))
         do_region_class_relabel(params.recur_level,section,params.stride,params.nb_sections,
                           region_relabel_pairs,pixel_data,temp_data);

#ifdef DEBUG
       if (params.debug > 3)
       {
         params.log_fs << endl << "After call to do_region_class_relabel, dump of the pixel data:" << endl << endl;
         for (pixel_index = 0; pixel_index < pixel_data_size; ++pixel_index)
         {
           pixel_data[pixel_index].print(pixel_index);
         }
       }
       if (params.debug > 3)
       {
         params.log_fs << endl << "List of pixel data labels:" << endl;
         for (pixel_index = 0; pixel_index < pixel_data_size; pixel_index++)
         {
           params.log_fs << "Element " << pixel_index;
           params.log_fs << " has region label ";
           pixel_data[pixel_index].print_region_label();
         }
       }
#endif

       if (params.gdissim_flag)
       {
         if ((params.dissim_crit == 1) || (params.dissim_crit == 2) || (params.dissim_crit == 3) ||
             (params.dissim_crit == 4) || (params.dissim_crit == 5) || (params.dissim_crit == 8) ||
             (params.dissim_crit == 10))
         {
           for (region_index = 0; region_index < region_classes_size; ++region_index)
             region_classes[region_index].set_sum_pixel_gdissim(0.0);
           update_sum_pixel_gdissim(params.recur_level,section,params.stride,params.nb_sections,
                                    pixel_data,region_classes,temp_data);
         }

         global_dissim = 0.0;
         numpix = 0.0;
         for (region_index = 0; region_index < region_classes_size; ++region_index)
         {
           if (region_classes[region_index].get_active_flag())
           {
             if ((params.dissim_crit == 6) || (params.dissim_crit == 7) || (params.dissim_crit == 9))
               region_classes[region_index].calc_sum_pixel_gdissim();
             region_numpix = region_classes[region_index].get_npix();
             numpix += region_numpix;
             global_dissim += region_classes[region_index].get_sum_pixel_gdissim();
           }
         }  
         if ((params.dissim_crit == 6) || (params.dissim_crit == 7))
         {
           global_dissim /= (numpix-1.0);
#ifdef MSE_SQRT
           global_dissim = sqrt(global_dissim); // Added to make dimensionality consistent
#endif
         }
         else
           global_dissim /= numpix;
       }

       spatial_data.update_region_label_map(params.recur_level,section,params.stride,
                                            params.nb_sections,pixel_data,
                                            temp_data);
       if (params.region_nb_objects_flag)
       {
         connected_component(params.recur_level, section, params.stride, params.nb_sections,
                             params.padded_ncols, params.padded_nrows, hlevel,
                             region_objects, spatial_data, temp_data);
         for (region_object_index = 0; region_object_index < region_objects_size; ++region_object_index)
           if (region_objects[region_object_index].get_active_flag())
             region_objects[region_object_index].clear();
         nb_objects = do_region_objects_init(params.recur_level,section,params.stride,
                                                  params.nb_sections, params.padded_ncols, params.padded_nrows,
                                                  pixel_data,spatial_data,region_objects,
                                                  temp_data);
         if ((params.std_dev_wght_flag) || (params.region_std_dev_flag))
           for (region_object_index = 0; region_object_index < region_objects_size; ++region_object_index)
             if (region_objects[region_object_index].get_active_flag())
               region_objects[region_object_index].calc_std_dev();
         update_region_object_info(params.recur_level,section,params.stride,params.nb_sections,
                                 hlevel,spatial_data,region_objects,temp_data);
         for (region_index = 0; region_index < region_classes_size; ++region_index)
           region_classes[region_index].clear_region_info();
         update_region_class_info(params.recur_level,section,params.stride,params.nb_sections,
                            hlevel,spatial_data,region_classes,temp_data);
#ifdef DEBUG
         if (params.debug > 3)
         {
           params.log_fs << "After updating region info, dump of region_classes:" << endl;
           for (region_index = 0; region_index < region_classes_size; ++region_index)
             region_classes[region_index].print(region_classes);
         }
#endif
       }
       else if ((params.region_boundary_npix_flag) || (params.boundary_map_flag))
       {
         for (region_index = 0; region_index < region_classes_size; ++region_index)
           region_classes[region_index].clear_region_info();
         boundary_map(params.recur_level,section,params.stride,params.nb_sections,
                      params.padded_ncols,params.padded_nrows,
                      hlevel,spatial_data,temp_data);
         update_region_class_info(params.recur_level,section,params.stride,params.nb_sections,
                            hlevel,spatial_data,region_classes,temp_data);
#ifdef DEBUG
         if (params.debug > 3)
         {
           params.log_fs << "After updating region info, dump of region_classes:" << endl;
           for (region_index = 0; region_index < region_classes_size; ++region_index)
             region_classes[region_index].print(region_classes);
         }
#endif
       }

       if (params.debug > 0)
       {
         params.log_fs << endl << "Data written at hlevel = " << hlevel << " for " << nregions << " region classes,";
         params.log_fs << " with maximum merging threshold = " << max_threshold << endl;
         if (params.gdissim_flag)
           params.log_fs << " and with global dissimilarity value = " << global_dissim << endl;
         if (params.region_nb_objects_flag)
           params.log_fs << "There are " << nb_objects << " region objects" << endl;
         params.log_fs << endl;
       }
       else
       {
         cout << endl << "Data written at hlevel = " << hlevel << " for " << nregions << " region classes,";
         cout << " with maximum merging threshold = " << max_threshold << endl;
         if (params.gdissim_flag)
           cout << " and with global dissimilarity value = " << global_dissim << endl;
         if (params.region_nb_objects_flag)
           cout << "There are " << nb_objects << " region objects" << endl;
         cout << endl;
       }
     // Write data.
       results_data.write(hlevel,nregions,nb_objects,region_classes,region_objects);
       oparams.int_buffer_size.push_back(results_data.get_int_buffer_index());
       oparams.max_threshold.push_back(max_threshold);
       if (params.gdissim_flag)
         oparams.gdissim.push_back(global_dissim);
#ifdef DEBUG
       if (params.debug > 3)
       {
         params.log_fs << endl << "After writing out results, dump of the region class data:" << endl << endl;
         for (region_index = 0; region_index < region_classes_size; ++region_index)
           if (region_classes[region_index].get_active_flag())
             region_classes[region_index].print(region_classes);
         if (params.region_objects_list_flag)
         {
           params.log_fs << endl << "After writing out results, dump of the region object data:" << endl << endl;
           for (region_index = 0; region_index < region_objects_size; ++region_index)
             if (region_objects[region_index].get_active_flag())
               region_objects[region_index].print(region_objects);
         }
         params.log_fs << endl << "After writing out results, dump of region classes map:" << endl << endl;
         spatial_data.print_class_labels_map(hlevel,region_classes);
         if (params.object_labels_map_flag)
         {
           params.log_fs << endl << "After writing out results, dump of region objects map:" << endl << endl;
           spatial_data.print_object_labels_map(hlevel,region_objects);
         }
       }
#endif
       hlevel++;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         region_classes[region_index].reset_merged_flag();
     } // if (hseg_out_flag)
     else if (prev_nregions == nregions)
       converge_nregions = nregions;
     prev_nregions = nregions;
   } // while (nregions > converge_nregions)

   region_classes_size = region_classes.size();

   results_data.close_output();
   if (params.boundary_map_flag)
   {
     spatial_data.open_boundary_map_output();
     spatial_data.write_boundary_map(temp_data);
     spatial_data.close_boundary_map();
   }

   oparams.level0_nb_classes = region_classes_size;
   if (params.region_nb_objects_flag)
     oparams.level0_nb_objects =  region_objects_size;
   oparams.nb_levels = hlevel;

   if (params.nb_sections > 1)
   {
     for (section = 0; section < params.nb_sections; section++)
     {
       remove_temp_file("input_image",section);
       remove_temp_file("mask",section);
       remove_temp_file("pixel",section);
       remove_temp_file("class_labels_map",section);
       remove_temp_file("object_labels_map",section);
       remove_temp_file("boundary_map",section);
     }
   }

// Write output parameters
     params.write_oparam(oparams);

   if (!params.gtkmm_flag)
     cout << "Processing is 100% complete" << endl;

   return true;
 }
} // namespace HSEGTilton
