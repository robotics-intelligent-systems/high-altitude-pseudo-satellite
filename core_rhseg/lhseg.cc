/*-----------------------------------------------------------
|
|  Routine Name: lhseg
|
|       Purpose: Performs hybrid of region growing and spectral clustering
|
|         Input: recur_level      (Current recursive level of this task)
|                section          (Section or window processed by this call to lrhseg)
|                stride           (Number of data sections (or tasks) covered by each child task)
|                nb_sections      (Number of data sections (or tasks) covered by this task)
|                converge_nregions(Number of regions below which to stop the region growing process)
|                pixel_data       (Class which holds information pertaining to the pixels processed by this task)
|                
|        Output: max_threshold    (Maximum merging threshold encountered)
|
|         Other: region_classes      (Class which holds region related information)
|                nregions         (Current number of regions)
|                temp_data        (buffers used in communications between parallel tasks)
|
|       Returns:
|
|    Written By: James C. Tilton, NASA's GSFC, Mail Code 606.3, Greenbelt, MD 20771
|        E-Mail: James.C.Tilton@nasa.gov
|
|       Written: September 17, 2002.
| Modifications: February 10, 2003 - Changed region_index to region_label
|                December 23, 2004 - Changed region label from short unsigned int to unsigned int
|                January 4, 2004 - Changed from sorting lists to utilizing a heap structure
|                May 31, 2005 - Added temporary file I/O for faster processing of large data sets
|                May 12, 2008 - Revised to work with globally defined Params and oParams class objects
|
------------------------------------------------------------*/

#include "hseg.h"
#include "params/params.h"
#include "pixel/pixel.h"
#include "region/region_class.h"
#include <iostream>
#ifdef DEBUG
#include <ctime>
#endif

extern HSEGTilton::Params params;

namespace HSEGTilton
{

 void lhseg(const short unsigned int& recur_level, const short unsigned int& section,
            const short unsigned int& stride, const short unsigned int& nb_sections,
            unsigned int& converge_nregions, unsigned int& nregions, double& max_threshold,
            vector<Pixel>& pixel_data, vector<RegionClass>& region_classes,
            vector<RegionClass *>& nghbr_heap, vector<RegionClass *>& region_heap,
            Temp& temp_data)
 {
#ifdef DEBUG
   time_t now;
#endif
   unsigned int region_index, cvg_nregions, prev_nregions;
   unsigned int iteration = 0, prev_print_it = 0;
   map<unsigned int,unsigned int> region_class_relabel_pairs;
   unsigned int heap_index;
   unsigned int region_classes_size = region_classes.size();
   unsigned int onregions = nregions;
   unsigned int nghbr_heap_size = nregions+1;
   unsigned int region_heap_size = 1;
   if (params.spclust_wght_flag)
     region_heap_size += nregions;

   if (nghbr_heap.size() < nghbr_heap_size)
     nghbr_heap.resize(nghbr_heap_size);
   if (region_heap.size() < region_heap_size)
     region_heap.resize(region_heap_size);

   nghbr_heap_size = 0;
   if (max_threshold == 0.0)
   {
    // Do one iteration of neighbor merges at max_threshold = 0.0.
    // This is the most efficient way to merge together a large homogeneous area.
     heap_index = 0;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
     {
       if (region_classes[region_index].get_active_flag())
       {
         region_classes[region_index].clear_best_merge();
         nghbr_heap[heap_index] = &region_classes[region_index];
         ++heap_index;
       }
     }
     nghbr_heap_size = nregions;
     nghbr_heap[nghbr_heap_size] = NULL;

     for (heap_index = 0; heap_index < nghbr_heap_size; ++heap_index)
       nghbr_heap[heap_index]->best_nghbr_init(region_classes);
     make_nghbr_heap(nghbr_heap, nghbr_heap_size);
     region_heap_size = 0;
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "After initializing regions, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         region_classes[region_index].print(region_classes);
     }
#endif
     if (nghbr_heap[0]->get_best_nghbr_dissim() == 0.0)
     {
       cvg_nregions = converge_nregions;
       if (merge_regions(false, cvg_nregions, nghbr_heap, nghbr_heap_size, 
                         region_heap, region_heap_size, region_classes, nregions, max_threshold))
         converge_nregions = cvg_nregions;
       if (onregions > nregions)
       {
         ++iteration;
         if (params.debug > 1)
         {
           params.log_fs << endl << "At iteration " << iteration << " lhseg converged";
           params.log_fs << " for neighbor merges with at the number of regions = " << nregions << endl;
           params.log_fs << "with maximum merging threshold = " << max_threshold;
#ifdef DEBUG
           now = time(NULL);
           params.log_fs << " at " << ctime(&now) << endl;
#else
           params.log_fs << endl;
#endif
         }
       }
     }
   } // if (max_threshold == 0)

   bool process_flag;
   if (recur_level > 1)
     process_flag = (nregions > converge_nregions);
   else
   {
     if (params.chk_nregions_flag)
       process_flag = (nregions > params.chk_nregions);
     else if (params.hseg_out_nregions_flag)
       process_flag = (nregions > params.hseg_out_nregions[0]);
     else // if (params.hseg_out_thresholds_flag)
       process_flag = (max_threshold < params.hseg_out_thresholds[0]);
   }
   process_flag = (process_flag && ((!params.spclust_wght_flag) || (nregions > params.spclust_start)));
   if (process_flag)
   {
    // Perform iterations of neighbor merges
     if (nghbr_heap_size == 0)
     {
      // Initialize nghbr_heap
       heap_index = 0;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
       {
         if (region_classes[region_index].get_active_flag())
         {
           region_classes[region_index].clear_best_merge();
           nghbr_heap[heap_index] = &region_classes[region_index];
           ++heap_index;
         }
       }
       nghbr_heap_size = nregions;
       nghbr_heap[nghbr_heap_size] = NULL;

       for (heap_index = 0; heap_index < nghbr_heap_size; ++heap_index)
         nghbr_heap[heap_index]->best_nghbr_init(region_classes);
       make_nghbr_heap(nghbr_heap, nghbr_heap_size);
     }
     region_heap_size = 0;
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "After initializing regions, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         region_classes[region_index].print(region_classes);
     }
#endif
     cvg_nregions = converge_nregions;
     if ((params.spclust_wght_flag) && (cvg_nregions < params.spclust_start))
       cvg_nregions = params.spclust_start;
     prev_nregions = nregions;
     while (process_flag)
     {
       ++iteration;
       if (merge_regions(false, cvg_nregions, nghbr_heap, nghbr_heap_size, 
                         region_heap, region_heap_size, region_classes, nregions, max_threshold))
         converge_nregions = cvg_nregions;
       if (recur_level > 1)
         process_flag = (nregions > converge_nregions);
       else
       {
         if (params.chk_nregions_flag)
           process_flag = (nregions > params.chk_nregions);
         else if (params.hseg_out_nregions_flag)
           process_flag = (nregions > params.hseg_out_nregions[0]);
         else // if (params.hseg_out_thresholds_flag)
          process_flag = (max_threshold < params.hseg_out_thresholds[0]);
       }
       if ((max_threshold >= FLT_MAX) || (prev_nregions == nregions))
         process_flag = false;

       prev_nregions = nregions;

       if ((iteration - prev_print_it) >= PRINT_INTERVAL)
       {
         prev_print_it = iteration;
         if (params.debug > 1)
         {
           params.log_fs << endl << "At iteration " << iteration << ", maximum threshold = " << max_threshold;
           params.log_fs << "," << endl << " and the number of regions = " << nregions;
#ifdef DEBUG
           now = time(NULL);
           params.log_fs << " at " << ctime(&now) << endl;
           unsigned int max_npix = 0;
           for (heap_index = 0; heap_index < nghbr_heap_size; ++heap_index)
             if (nghbr_heap[heap_index]->get_npix() > max_npix)
               max_npix = nghbr_heap[heap_index]->get_npix();
           params.log_fs << "Largest region has " << max_npix << " pixels." << endl;
#else
           params.log_fs << endl;
#endif
         }
       }
     } // while (process_flag)

     if (params.spclust_wght_flag)
     {
       if (params.debug > 1)
       {
         params.log_fs << endl << "At iteration " << iteration << " lhseg converged";
         params.log_fs << " for neighbor merges";
         if (iteration != prev_print_it)
         {
           params.log_fs << ", maximum merging threshold = " << max_threshold << endl;
           params.log_fs << "and the number of regions = " << nregions;
#ifdef DEBUG
           now = time(NULL);
           params.log_fs << " at " << ctime(&now) << endl;
#else
           params.log_fs << endl;
#endif
         }
         else
           params.log_fs << endl;
       }
     }
   } // if (process_flag)

   process_flag = false;
   if (recur_level > 1)
     process_flag = (nregions > converge_nregions);
   else
   {
     if (params.chk_nregions_flag)
       process_flag = (nregions > params.chk_nregions);
     else if (params.hseg_out_nregions_flag)
       process_flag = (nregions > params.hseg_out_nregions[0]);
     else // if (params.hseg_out_thresholds_flag)
       process_flag = (max_threshold < params.hseg_out_thresholds[0]);
   }
   if ((process_flag) && params.spclust_wght_flag)
   {
    // Perform a combination of spectral clustering merges and region neighbor merges
    // Initialize nghbr_heap and region_heap
     heap_index = 0;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
     {
       if (region_classes[region_index].get_active_flag())
       {
         region_classes[region_index].clear_best_merge();
         nghbr_heap[heap_index] = &region_classes[region_index];
         region_heap[heap_index] = &region_classes[region_index];
         ++heap_index;
       }
     }
     nghbr_heap_size = nregions;
     nghbr_heap[nghbr_heap_size] = NULL;
     region_heap_size = nregions;
     region_heap[region_heap_size] = NULL;

     for (heap_index = 0; heap_index < nghbr_heap_size; ++heap_index)
       nghbr_heap[heap_index]->best_nghbr_init(region_classes);
     make_nghbr_heap(nghbr_heap, nghbr_heap_size);

     for (heap_index = 0; heap_index < region_heap_size; ++heap_index)
       region_heap[heap_index]->best_region_init(heap_index,
                                                 region_heap,region_heap_size);
     make_region_heap(region_heap, region_heap_size);
#ifdef DEBUG
     if (params.debug > 3)
     {
       params.log_fs << endl << "After initializing regions, dump of the region data:" << endl << endl;
       for (region_index = 0; region_index < region_classes_size; ++region_index)
         region_classes[region_index].print(region_classes);
     }
#endif
     cvg_nregions = converge_nregions;
     prev_nregions = nregions;
     while (process_flag)
     {
       ++iteration;
       if (merge_regions(false, cvg_nregions, nghbr_heap, nghbr_heap_size, 
                         region_heap, region_heap_size, region_classes, nregions, max_threshold))
         converge_nregions = cvg_nregions;
       process_flag = false;
       if (recur_level > 1)
         process_flag = (nregions > converge_nregions);  
       else
       {
         if (params.chk_nregions_flag)
           process_flag = (nregions > params.chk_nregions);
         else if (params.hseg_out_nregions_flag)
           process_flag = (nregions > params.hseg_out_nregions[0]);
         else // if (params.hseg_out_thresholds_flag)
           process_flag = (max_threshold < params.hseg_out_thresholds[0]);
       }
       if ((max_threshold >= FLT_MAX) || (prev_nregions == nregions))
         process_flag = false;

       prev_nregions = nregions;

       if ((iteration - prev_print_it) >= PRINT_INTERVAL)
       {
         prev_print_it = iteration;
         if (params.debug > 1)
         {
           params.log_fs << endl << "At iteration " << iteration << ", maximum threshold = " << max_threshold;
           params.log_fs << "," << endl << " and the number of regions = " << nregions;
#ifdef DEBUG
           now = time(NULL);
           params.log_fs << " at " << ctime(&now) << endl;
           unsigned int max_npix = 0;
           for (heap_index = 0; heap_index < nghbr_heap_size; ++heap_index)
             if (nghbr_heap[heap_index]->get_npix() > max_npix)
               max_npix = nghbr_heap[heap_index]->get_npix();
           params.log_fs << "Largest region has " << max_npix << " pixels." << endl;
#else
           params.log_fs << endl;
#endif
         }
       }
     } // while (process_flag)
   } // if (process_flag)

   if (params.debug > 1)
   {
     params.log_fs << endl << "At iteration " << iteration << " lhseg converged";
     if (iteration != prev_print_it)
     {
       params.log_fs << ", maximum merging threshold = " << max_threshold << endl;
       params.log_fs << "and the number of regions = " << nregions;
#ifdef DEBUG
       now = time(NULL);
       params.log_fs << " at " << ctime(&now) << endl;
#else
       params.log_fs << endl;
#endif
     }
     else
       params.log_fs << endl;
   }
#ifdef DEBUG
   if (params.debug > 3)
   {
     params.log_fs << endl << "Exiting merge_region loop, dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
       region_classes[region_index].print(region_classes);
   }
#endif
   if (onregions != nregions)
   {
    // Update pixel_data.region_label based on merges performed this call to lhseg.
     unsigned int region_label, merge_region_index, merge_region_label;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
     {
       merge_region_index = region_index;
       merge_region_label = region_classes[merge_region_index].get_merge_region_label();
       if (merge_region_label != 0)
       {
         while (merge_region_label != 0)
         {
           merge_region_index = merge_region_label - 1;
           merge_region_label = region_classes[merge_region_index].get_merge_region_label();
         }
         region_classes[region_index].set_merge_region_label(region_classes[merge_region_index].get_label());
       }
     }

     region_class_relabel_pairs.clear();
     for (region_index = 0; region_index < region_classes_size; ++region_index)
     {
       region_label = region_index + 1;
       merge_region_label = region_classes[region_index].get_merge_region_label();
       if (merge_region_label != 0)
         region_class_relabel_pairs.insert(make_pair(region_label,merge_region_label));
     }
     if (!(region_class_relabel_pairs.empty()))
       do_region_class_relabel(recur_level,section,stride,nb_sections,region_class_relabel_pairs,
                         pixel_data,temp_data);
     region_class_relabel_pairs.clear();

     for (region_index = 0; region_index < region_classes_size; ++region_index)
       region_classes[region_index].set_merge_region_label(0);
   } // if (onregions != nregions)
#ifdef DEBUG
   if (params.debug > 3)
   {
     params.log_fs << endl << "Exiting lhseg, dump of the region data:" << endl << endl;
     for (region_index = 0; region_index < region_classes_size; ++region_index)
       region_classes[region_index].print(region_classes);
   }
   if (params.debug > 2)
   {
     params.log_fs << "Completed region relabeling within lhseg" << endl;
   }
#endif
   return;
 }

} // namespace HSEGTilton

